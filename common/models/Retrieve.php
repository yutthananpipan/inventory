<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%retrieve}}".
 *
 * @property int $item_id
 * @property int $location_id
 * @property int $project_id
 * @property int $qty
 * @property string|null $description
 * @property string $retrieve_date
 * @property string $retrieve_name
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property Item $item
 * @property Location $location
 * @property Project $project
 */
class Retrieve extends \yii\db\ActiveRecord
{
    public $m1;
    public $m2;
    public $m3;
    public $m4;
    public $m5;
    public $m6;
    public $m7;
    public $m8;
    public $m9;
    public $m10;
    public $m11;
    public $m12;

    public $sum;
    public $cnt;

    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%retrieve}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'location_id', 'project_id', 'qty', 'retrieve_date', 'retrieve_name'], 'required'],
            [['item_id', 'location_id', 'project_id', 'qty', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['retrieve_date'], 'safe'],
            [['retrieve_name'], 'string', 'max' => 255],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'pro_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'location_id' => 'Location ID',
            'project_id' => 'Project ID',
            'qty' => 'Qty',
            'description' => 'Description',
            'retrieve_date' => 'Retrieve Date',
            'retrieve_name' => 'Retrieve Name',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[Item]].
     *
     * @return \yii\db\ActiveQuery|ItemQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * Gets query for [[Location]].
     *
     * @return \yii\db\ActiveQuery|LocationQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id']);
    }

    /**
     * Gets query for [[Project]].
     *
     * @return \yii\db\ActiveQuery|ProjectQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['pro_id' => 'project_id']);
    }

    /**
     * {@inheritdoc}
     * @return RetrieveQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RetrieveQuery(get_called_class());
    }
}
