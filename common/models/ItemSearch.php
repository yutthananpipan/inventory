<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Item;

/**
 * ItemSearch represents the model behind the search form of `common\models\Item`.
 */
class ItemSearch extends Item
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'sub_category_id', 'qty', 'receive_date', 'supplier_id', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['name', 'item_code', 'mfr_part', 'company_part', 'description', 'currency', 'measure', 'remark', 'ref'], 'safe'],
            [['cost', 'price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Item::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cost' => $this->cost,
            'price' => $this->price,
            'category_id' => $this->category_id,
            'sub_category_id' => $this->sub_category_id,
            'qty' => $this->qty,
            'receive_date' => $this->receive_date,
            'supplier_id' => $this->supplier_id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'item_code', $this->item_code])
            ->andFilterWhere(['like', 'mfr_part', $this->mfr_part])
            ->andFilterWhere(['like', 'company_part', $this->company_part])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'measure', $this->measure])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'ref', $this->ref]);

        $query->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }

    public function searchitem($params)
    {
        $query = Item::find();
        $query->join('LEFT JOIN', 'category', 'category.id = item.category_id');
        $query->join('LEFT JOIN', 'sub_category', 'sub_category.id = item.sub_category_id');

        $query->orFilterWhere(['like', 'item.name', $params])
            ->orFilterWhere(['like', 'item.item_code', $params])
            ->orFilterWhere(['like', 'item.mfr_part', $params])
            ->orFilterWhere(['like', 'item.company_part', $params])
            ->orFilterWhere(['like', 'item.description', $params])
            ->orFilterWhere(['like', 'item.remark', $params])
            ->orFilterWhere(['like', 'category.name', $params])
            ->orFilterWhere(['like', 'sub_category.name', $params]);

        $query->orderBy(['item.id' => SORT_DESC]);
        $query->limit(100);
        $query->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
