<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Retrieve]].
 *
 * @see Retrieve
 */
class RetrieveQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Retrieve[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Retrieve|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
