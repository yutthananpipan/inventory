<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%project}}".
 *
 * @property int $pro_id
 * @property string|null $project_no
 * @property string $pro_name
 * @property string|null $project_desc
 *
 * @property Retreive[] $retreives
 * @property Item[] $items
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pro_name'], 'required'],
            [['project_no'], 'string', 'max' => 32],
            [['pro_name', 'project_desc'], 'string', 'max' => 255],
            [['project_no'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pro_id' => 'Pro ID',
            'project_no' => 'Project No',
            'pro_name' => 'Pro Name',
            'project_desc' => 'Project Desc',
        ];
    }

    /**
     * Gets query for [[Retreives]].
     *
     * @return \yii\db\ActiveQuery|RetreiveQuery
     */
    public function getRetreives()
    {
        return $this->hasMany(Retreive::className(), ['project_id' => 'pro_id']);
    }

    /**
     * Gets query for [[Items]].
     *
     * @return \yii\db\ActiveQuery|ItemQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['id' => 'item_id'])->viaTable('{{%retreive}}', ['project_id' => 'pro_id']);
    }

    /**
     * {@inheritdoc}
     * @return ProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProjectQuery(get_called_class());
    }
}
