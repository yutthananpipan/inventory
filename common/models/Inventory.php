<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%inventory}}".
 *
 * @property int $item_id
 * @property int $location_id
 * @property int $qty
 * @property int $ideal_qty
 * @property int $warn_qty
 * @property string|null $barcode
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property Item $item
 * @property Location $location
 */
class Inventory extends \yii\db\ActiveRecord
{
    public $sum_qty;

    public $sum_ideal_qty;
    public $sum_warn_qty;

    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%inventory}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'location_id', 'qty'], 'required'],
            [['item_id', 'location_id', 'qty', 'ideal_qty', 'warn_qty', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['barcode'], 'string'],
            [['item_id', 'location_id'], 'unique', 'targetAttribute' => ['item_id', 'location_id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['location_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'location_id' => 'Location ID',
            'qty' => 'Qty',
            'ideal_qty' => 'Ideal Qty',
            'warn_qty' => 'Warn Qty',
            'barcode' => 'Barcode',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[Item]].
     *
     * @return \yii\db\ActiveQuery|ItemQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * Gets query for [[Location]].
     *
     * @return \yii\db\ActiveQuery|LocationQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id']);
    }

    /**
     * {@inheritdoc}
     * @return InventoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InventoryQuery(get_called_class());
    }
}
