<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[RFID]].
 *
 * @see RFID
 */
class RFIDQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return RFID[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return RFID|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
