<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%item}}".
 *
 * @property int $id
 * @property string $name
 * @property string|null $item_code
 * @property string|null $mfr_part
 * @property string|null $company_part
 * @property string|null $description
 * @property float|null $cost
 * @property float|null $price
 * @property string|null $currency
 * @property int $category_id
 * @property int|null $sub_category_id
 * @property int|null $qty จำนวนสินค้าเข้า
 * @property string|null $measure
 * @property string|null $remark
 * @property string|null $ref
 * @property string|null $receive_date
 * @property int|null $supplier_id
 * @property int|null $created_by
 * @property int|null $created_at
 * @property int|null $updated_by
 * @property int|null $updated_at
 *
 * @property Inventory[] $inventories
 * @property Category $category
 * @property SubCategory $subCategory
 * @property Supplier $supplier
 */
class Item extends \yii\db\ActiveRecord
{
    const UPLOAD_FOLDER = 'img/item';

    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%item}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'category_id'], 'required'],
            [['cost', 'price'], 'number'],
            [['category_id', 'sub_category_id', 'qty', 'supplier_id', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['remark'], 'string'],
            [['receive_date'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
            [['item_code', 'mfr_part', 'company_part'], 'string', 'max' => 32],
            [['currency'], 'string', 'max' => 3],
            [['measure', 'ref'], 'string', 'max' => 50],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['sub_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubCategory::className(), 'targetAttribute' => ['sub_category_id' => 'id']],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'item_code' => 'Item Code',
            'mfr_part' => 'Mfr Part',
            'company_part' => 'Company Part',
            'description' => 'Description',
            'cost' => 'Cost',
            'price' => 'Price',
            'currency' => 'Currency',
            'category_id' => 'Category ID',
            'sub_category_id' => 'Sub Category ID',
            'qty' => 'จำนวนสินค้าเข้า',
            'measure' => 'Measure',
            'remark' => 'Remark',
            'ref' => 'Ref',
            'receive_date' => 'Receive Date',
            'supplier_id' => 'Supplier ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Inventories]].
     *
     * @return \yii\db\ActiveQuery|InventoryQuery
     */
    public function getInventories()
    {
        return $this->hasMany(Inventory::className(), ['item_id' => 'id']);
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery|CategoryQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[SubCategory]].
     *
     * @return \yii\db\ActiveQuery|SubCategoryQuery
     */
    public function getSubCategory()
    {
        return $this->hasOne(SubCategory::className(), ['id' => 'sub_category_id']);
    }

    /**
     * Gets query for [[Supplier]].
     *
     * @return \yii\db\ActiveQuery|SupplierQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplier_id']);
    }

    /**
     * {@inheritdoc}
     * @return ItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ItemQuery(get_called_class());
    }

    public static function getUploadUrl()
    {
        return Url::base(true) . '/' . self::UPLOAD_FOLDER . '/';
    }

    public static function getUploadPath()
    {
        return Yii::getAlias('@webroot') . '/' . self::UPLOAD_FOLDER . '/';
    }

    public function isImage($filePath)
    {
        return @is_array(getimagesize($filePath)) ? true : false;
    }

    public function getThumbnail($ref, $item_name)
    {
        $preview = ['url' => '', 'src' => '', 'options' => ['class' => 'img-fluid']];
        try {
            $upload = Uploads::find()->where(['ref' => $ref])->one();

            $filePath = self::getUploadPath() . $ref . '/thumbnail/' . $upload->real_filename;
            $isImage = $this->isImage($filePath);
            if ($isImage) {
                $preview = [
                    'url' => (!$isImage) ? self::getUploadUrl(true) . '' : self::getUploadUrl() . $ref . '/' . $upload->real_filename,
                    'src' => self::getUploadUrl(true) . $ref . '/thumbnail/' . $upload->real_filename,
                    'options' => ['title' => $item_name]
                ];
            }
        } catch (\Exception $e) {

        }
        return $preview;
    }

    public function getThumbnails($ref, $item_name, $limit)
    {
        $preview = [];
        try {
            $upload = Uploads::find()->where(['ref' => $ref])->limit($limit)->all();

            foreach ($upload as $file) {
                $filePath = self::getUploadPath() . $ref . '/thumbnail/' . $file->real_filename;
                $isImage = $this->isImage($filePath);
                if ($isImage) {
                    $preview[] = [
                        'url' => (!$isImage) ? self::getUploadUrl(true) . '' : self::getUploadUrl() . $ref . '/' . $file->real_filename,
                        'src' => self::getUploadUrl(true) . $ref . '/thumbnail/' . $file->real_filename,
                        'options' => ['title' => $item_name]
                    ];
                }
            }
        } catch (\Exception $e) {

        }
        return $preview;
    }

}
