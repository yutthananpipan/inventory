<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%supplier}}".
 *
 * @property int $id
 * @property string $supplier_name
 * @property string|null $contact_persion
 * @property string|null $contact_firstname
 * @property string|null $address
 * @property string|null $phone_primary
 * @property string|null $phone_alternative
 * @property string|null $fax
 * @property string|null $email
 *
 * @property Item[] $items
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%supplier}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplier_name'], 'required'],
            [['address'], 'string'],
            [['supplier_name', 'contact_persion', 'contact_firstname', 'email'], 'string', 'max' => 255],
            [['phone_primary', 'phone_alternative', 'fax'], 'string', 'max' => 15],
            [['supplier_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supplier_name' => 'Supplier Name',
            'contact_persion' => 'Contact Person',
            'contact_firstname' => 'Contact Firstname',
            'address' => 'Address',
            'phone_primary' => 'Phone Primary',
            'phone_alternative' => 'Phone Alternative',
            'fax' => 'Fax',
            'email' => 'Email',
        ];
    }

    /**
     * Gets query for [[Items]].
     *
     * @return \yii\db\ActiveQuery|ItemQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['supplier_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return SupplierQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SupplierQuery(get_called_class());
    }
}
