-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.40-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for inventory
CREATE DATABASE IF NOT EXISTS `inventory` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `inventory`;

-- Dumping structure for table inventory.auth_assignment
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `idx-auth_assignment-user_id` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table inventory.auth_assignment: ~3 rows (approximately)
DELETE FROM `auth_assignment`;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
	('Administrator', '1', 1601607966),
	('Administrator', '4', 1601602254),
	('Administrator', '5', 1601608006),
	('User', '2', 1600234508),
	('Viewer', '3', 1600242081);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;

-- Dumping structure for table inventory.auth_item
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table inventory.auth_item: ~41 rows (approximately)
DELETE FROM `auth_item`;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
	('/admin/*', 2, NULL, NULL, NULL, 1600763691, 1600763691),
	('/category/*', 2, NULL, NULL, NULL, 1600769275, 1600769275),
	('/category/index', 2, NULL, NULL, NULL, 1600835539, 1600835539),
	('/category/view', 2, NULL, NULL, NULL, 1600769268, 1600769268),
	('/gii/*', 2, NULL, NULL, NULL, 1600233069, 1600233069),
	('/item/*', 2, NULL, NULL, NULL, 1600767371, 1600767371),
	('/item/index', 2, NULL, NULL, NULL, 1600835522, 1600835522),
	('/item/retrieve-report', 2, NULL, NULL, NULL, 1601529776, 1601529776),
	('/item/view', 2, NULL, NULL, NULL, 1600767391, 1600767391),
	('/location/*', 2, NULL, NULL, NULL, 1600769299, 1600769299),
	('/location/index', 2, NULL, NULL, NULL, 1600835548, 1600835548),
	('/location/view', 2, NULL, NULL, NULL, 1600769305, 1600769305),
	('/project/*', 2, NULL, NULL, NULL, 1600769125, 1600769125),
	('/project/index', 2, NULL, NULL, NULL, 1600835532, 1600835532),
	('/project/view', 2, NULL, NULL, NULL, 1600769140, 1600769140),
	('/retreive/*', 2, NULL, NULL, NULL, 1600769357, 1600769357),
	('/retreive/view', 2, NULL, NULL, NULL, 1600769363, 1600769363),
	('/site/*', 2, NULL, NULL, NULL, 1600233127, 1600233127),
	('/site/index', 2, NULL, NULL, NULL, 1600763829, 1600763829),
	('/supplier/*', 2, NULL, NULL, NULL, 1600769330, 1600769330),
	('/supplier/index', 2, NULL, NULL, NULL, 1600835559, 1600835559),
	('/supplier/view', 2, NULL, NULL, NULL, 1600769335, 1600769335),
	('/user/*', 2, NULL, NULL, NULL, 1600233102, 1600233102),
	('/user/admin/*', 2, NULL, NULL, NULL, 1600763728, 1600763728),
	('Administrator', 1, 'สามารถเข้าถึงข้อมูลและจัดการได้ทุกอย่าง', NULL, NULL, 1600232830, 1601533120),
	('manage-category', 2, NULL, NULL, NULL, 1600765437, 1601533166),
	('manage-inventory', 2, NULL, NULL, NULL, 1600765486, 1600769408),
	('manage-item', 2, NULL, NULL, NULL, 1600765343, 1600767405),
	('manage-location', 2, NULL, NULL, NULL, 1600765515, 1600769432),
	('manage-project', 2, NULL, NULL, NULL, 1600765541, 1601533191),
	('manage-retreive', 2, NULL, NULL, NULL, 1600765570, 1600769463),
	('manage-site', 2, 'Can edit site controller', NULL, NULL, 1600765238, 1600765238),
	('manage-subcategory', 2, NULL, NULL, NULL, 1600765593, 1600765593),
	('manage-supplier', 2, NULL, NULL, NULL, 1600765618, 1600769505),
	('User', 1, 'Create, Update, Delete data / Can not manage user', NULL, NULL, 1600232964, 1601533132),
	('user-admin', 2, 'manage user', NULL, NULL, 1600765287, 1600765287),
	('view-category', 2, NULL, NULL, NULL, 1600765417, 1600835584),
	('view-inventory', 2, NULL, NULL, NULL, 1600765470, 1600769540),
	('view-item', 2, NULL, NULL, NULL, 1600765323, 1601529820),
	('view-location', 2, NULL, NULL, NULL, 1600765500, 1600835670),
	('view-project', 2, NULL, NULL, NULL, 1600765531, 1600835642),
	('view-retreive', 2, NULL, NULL, NULL, 1600765552, 1600769599),
	('view-site', 2, 'can view site', NULL, NULL, 1600765187, 1600767518),
	('view-subcategory', 2, NULL, NULL, NULL, 1600765582, 1600765582),
	('view-supplier', 2, NULL, NULL, NULL, 1600765609, 1600835695),
	('Viewer', 1, 'view only', NULL, NULL, 1600232984, 1601529791);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;

-- Dumping structure for table inventory.auth_item_child
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table inventory.auth_item_child: ~43 rows (approximately)
DELETE FROM `auth_item_child`;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
	('Administrator', '/admin/*'),
	('Administrator', '/gii/*'),
	('Administrator', 'User'),
	('Administrator', 'user-admin'),
	('manage-category', '/category/*'),
	('manage-item', '/item/*'),
	('manage-location', '/location/*'),
	('manage-project', '/project/*'),
	('manage-retreive', '/retreive/*'),
	('manage-site', '/site/*'),
	('manage-supplier', '/supplier/*'),
	('User', '/site/*'),
	('User', 'manage-category'),
	('User', 'manage-inventory'),
	('User', 'manage-item'),
	('User', 'manage-location'),
	('User', 'manage-project'),
	('User', 'manage-retreive'),
	('User', 'manage-site'),
	('User', 'manage-subcategory'),
	('User', 'manage-supplier'),
	('User', 'Viewer'),
	('user-admin', '/user/*'),
	('user-admin', '/user/admin/*'),
	('view-category', '/category/index'),
	('view-category', '/category/view'),
	('view-item', '/item/index'),
	('view-item', '/item/retrieve-report'),
	('view-item', '/item/view'),
	('view-location', '/location/index'),
	('view-location', '/location/view'),
	('view-project', '/project/index'),
	('view-project', '/project/view'),
	('view-retreive', '/retreive/view'),
	('view-site', '/site/index'),
	('view-supplier', '/supplier/index'),
	('view-supplier', '/supplier/view'),
	('Viewer', 'view-category'),
	('Viewer', 'view-inventory'),
	('Viewer', 'view-item'),
	('Viewer', 'view-location'),
	('Viewer', 'view-project'),
	('Viewer', 'view-retreive'),
	('Viewer', 'view-site'),
	('Viewer', 'view-subcategory'),
	('Viewer', 'view-supplier');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;

-- Dumping structure for table inventory.auth_rule
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table inventory.auth_rule: ~0 rows (approximately)
DELETE FROM `auth_rule`;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;

-- Dumping structure for table inventory.category
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.category: ~13 rows (approximately)
DELETE FROM `category`;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`, `name`) VALUES
	(1, 'Amp'),
	(5, 'Board Charger  '),
	(6, 'Capacitor'),
	(7, 'DesktopCase'),
	(8, 'Inductor'),
	(9, 'Misc'),
	(10, 'OSC'),
	(16, 'Power Amp'),
	(11, 'Power Amplifier'),
	(3, 'Power Supply '),
	(14, 'RackCase'),
	(2, 'Resistor'),
	(15, 'RF Connector'),
	(4, 'Semiconductor'),
	(22, 'ทั่วไป'),
	(23, 'สายอากาศ');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table inventory.currency
CREATE TABLE IF NOT EXISTS `currency` (
  `name` varchar(20) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL,
  `symbol` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.currency: ~113 rows (approximately)
DELETE FROM `currency`;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` (`name`, `code`, `symbol`) VALUES
	('Leke', 'ALL', 'Lek'),
	('Dollars', 'USD', '$'),
	('Afghanis', 'AFN', '؋'),
	('Pesos', 'ARS', '$'),
	('Guilders', 'AWG', 'ƒ'),
	('Dollars', 'AUD', '$'),
	('New Manats', 'AZN', 'ман'),
	('Dollars', 'BSD', '$'),
	('Dollars', 'BBD', '$'),
	('Rubles', 'BYR', 'p.'),
	('Euro', 'EUR', '€'),
	('Dollars', 'BZD', 'BZ$'),
	('Dollars', 'BMD', '$'),
	('Bolivianos', 'BOB', '$b'),
	('Convertible Marka', 'BAM', 'KM'),
	('Pula', 'BWP', 'P'),
	('Leva', 'BGN', 'лв'),
	('Reais', 'BRL', 'R$'),
	('Pounds', 'GBP', '£'),
	('Dollars', 'BND', '$'),
	('Riels', 'KHR', '៛'),
	('Dollars', 'CAD', '$'),
	('Dollars', 'KYD', '$'),
	('Pesos', 'CLP', '$'),
	('Yuan Renminbi', 'CNY', '¥'),
	('Pesos', 'COP', '$'),
	('Colón', 'CRC', '₡'),
	('Kuna', 'HRK', 'kn'),
	('Pesos', 'CUP', '₱'),
	('Koruny', 'CZK', 'Kč'),
	('Kroner', 'DKK', 'kr'),
	('Pesos', 'DOP', 'RD$'),
	('Dollars', 'XCD', '$'),
	('Pounds', 'EGP', '£'),
	('Colones', 'SVC', '$'),
	('Pounds', 'FKP', '£'),
	('Dollars', 'FJD', '$'),
	('Cedis', 'GHC', '¢'),
	('Pounds', 'GIP', '£'),
	('Quetzales', 'GTQ', 'Q'),
	('Pounds', 'GGP', '£'),
	('Dollars', 'GYD', '$'),
	('Lempiras', 'HNL', 'L'),
	('Dollars', 'HKD', '$'),
	('Forint', 'HUF', 'Ft'),
	('Kronur', 'ISK', 'kr'),
	('Rupees', 'INR', 'Rp'),
	('Rupiahs', 'IDR', 'Rp'),
	('Rials', 'IRR', '﷼'),
	('Pounds', 'IMP', '£'),
	('New Shekels', 'ILS', '₪'),
	('Dollars', 'JMD', 'J$'),
	('Yen', 'JPY', '¥'),
	('Pounds', 'JEP', '£'),
	('Tenge', 'KZT', 'лв'),
	('Won', 'KPW', '₩'),
	('Won', 'KRW', '₩'),
	('Soms', 'KGS', 'лв'),
	('Kips', 'LAK', '₭'),
	('Lati', 'LVL', 'Ls'),
	('Pounds', 'LBP', '£'),
	('Dollars', 'LRD', '$'),
	('Switzerland Francs', 'CHF', 'CHF'),
	('Litai', 'LTL', 'Lt'),
	('Denars', 'MKD', 'ден'),
	('Ringgits', 'MYR', 'RM'),
	('Rupees', 'MUR', '₨'),
	('Pesos', 'MXN', '$'),
	('Tugriks', 'MNT', '₮'),
	('Meticais', 'MZN', 'MT'),
	('Dollars', 'NAD', '$'),
	('Rupees', 'NPR', '₨'),
	('Guilders', 'ANG', 'ƒ'),
	('Dollars', 'NZD', '$'),
	('Cordobas', 'NIO', 'C$'),
	('Nairas', 'NGN', '₦'),
	('Krone', 'NOK', 'kr'),
	('Rials', 'OMR', '﷼'),
	('Rupees', 'PKR', '₨'),
	('Balboa', 'PAB', 'B/.'),
	('Guarani', 'PYG', 'Gs'),
	('Nuevos Soles', 'PEN', 'S/.'),
	('Pesos', 'PHP', 'Php'),
	('Zlotych', 'PLN', 'zł'),
	('Rials', 'QAR', '﷼'),
	('New Lei', 'RON', 'lei'),
	('Rubles', 'RUB', 'руб'),
	('Pounds', 'SHP', '£'),
	('Riyals', 'SAR', '﷼'),
	('Dinars', 'RSD', 'Дин.'),
	('Rupees', 'SCR', '₨'),
	('Dollars', 'SGD', '$'),
	('Dollars', 'SBD', '$'),
	('Shillings', 'SOS', 'S'),
	('Rand', 'ZAR', 'R'),
	('Rupees', 'LKR', '₨'),
	('Kronor', 'SEK', 'kr'),
	('Dollars', 'SRD', '$'),
	('Pounds', 'SYP', '£'),
	('New Dollars', 'TWD', 'NT$'),
	('Baht', 'THB', '฿'),
	('Dollars', 'TTD', 'TT$'),
	('Lira', 'TRY', '₺'),
	('Liras', 'TRL', '£'),
	('Dollars', 'TVD', '$'),
	('Hryvnia', 'UAH', '₴'),
	('Pesos', 'UYU', '$U'),
	('Sums', 'UZS', 'лв'),
	('Bolivares Fuertes', 'VEF', 'Bs'),
	('Dong', 'VND', '₫'),
	('Rials', 'YER', '﷼'),
	('Zimbabwe Dollars', 'ZWD', 'Z$'),
	('Rupees', 'INR', '₹');
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;

-- Dumping structure for table inventory.inventory
CREATE TABLE IF NOT EXISTS `inventory` (
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `ideal_qty` int(11) NOT NULL DEFAULT '0',
  `warn_qty` int(11) NOT NULL DEFAULT '0',
  `barcode` text,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_id`,`location_id`),
  KEY `FK_inventory_location` (`location_id`),
  CONSTRAINT `FK_inventory_item` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_inventory_location` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.inventory: ~18 rows (approximately)
DELETE FROM `inventory`;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
INSERT INTO `inventory` (`item_id`, `location_id`, `qty`, `ideal_qty`, `warn_qty`, `barcode`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(2, 2, 2000, 500, 100, NULL, 1601294245, 1, 1601294245, 1),
	(2, 3, 530, 30, 10, NULL, 1601294245, 1, 1601294245, 1),
	(4, 2, 8096, 500, 100, NULL, 1598237435, 1, 0, 0),
	(9, 5, 45, 0, 0, NULL, 1600915841, 1, 0, 0),
	(10, 2, 3500, 500, 100, NULL, 1600915841, 1, 0, 0),
	(20, 7, 90, 0, 0, NULL, 1601543651, 1, 1601544368, 1),
	(21, 1, 9, 0, 0, NULL, 1601547781, 2, 1601547847, 2);
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;

-- Dumping structure for table inventory.item
CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item_code` varchar(32) DEFAULT NULL,
  `mfr_part` varchar(32) DEFAULT NULL,
  `company_part` varchar(32) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL COMMENT 'จำนวนสินค้าเข้า',
  `measure` varchar(50) DEFAULT NULL,
  `remark` text,
  `ref` varchar(50) DEFAULT NULL,
  `receive_date` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_item_category` (`category_id`),
  KEY `FK_item_sub_category` (`sub_category_id`),
  KEY `FK_item_supplier` (`supplier_id`),
  CONSTRAINT `FK_item_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_item_sub_category` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_category` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_item_supplier` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.item: ~9 rows (approximately)
DELETE FROM `item`;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` (`id`, `name`, `item_code`, `mfr_part`, `company_part`, `description`, `cost`, `price`, `currency`, `category_id`, `sub_category_id`, `qty`, `measure`, `remark`, `ref`, `receive_date`, `supplier_id`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
	(2, '0.1 nF 50v', '', 'LF390811UNSPSC', 'LFSH1435', '0.1 nF 50v', 12.00, 2.00, 'USD', 6, 10, 2530, 'Pcs', '', '1', NULL, 2, NULL, NULL, 1, 1601294245),
	(4, '0E-1210', NULL, 'LF390811UNSPSC', 'LFSH1435', '0E,1/3W,1210', 11.00, 2.00, 'USD', 2, 32, 8096, 'ตัว', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, '1U-BLNK-DRC', NULL, 'LF390811UNSPSC', 'LFSH1435', '1U blank Plate DRC', 33.00, 3.00, 'THB', 1, NULL, 45, NULL, NULL, '3', '2020-09-27', NULL, NULL, NULL, NULL, NULL),
	(10, '1 pF 2012', NULL, 'LF390811UNSPSC', 'LFSH1435', '1 pF 2012', 3.00, 4.00, 'THB', 6, 10, 3500, 'Pcs', NULL, '4', '2020-09-27', NULL, NULL, NULL, NULL, NULL),
	(20, 'เหล็ก', '0000000-11111111111', 'LF390811UNSPSC', 'LFSH1435', '', 500.00, 10000.00, 'THB', 22, NULL, 100, 'ท่อน', '', 'wuEdfWgTUQ5-EW6mF3WnXm', '2020-10-02', 2, 1, 1601543448, 1, 1601543651),
	(21, 'สายอากาศ', '0000000-11111111111', 'LF390811UNSPSC', 'LFSH1435', '', NULL, NULL, 'USD', 22, NULL, 10, '', '', 'RbynO3e_HXUMuYDVr6l2nQ', NULL, NULL, 2, 1601547696, 2, 1601547781);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;

-- Dumping structure for table inventory.location
CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `address` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.location: ~6 rows (approximately)
DELETE FROM `location`;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` (`id`, `name`, `address`) VALUES
	(1, 'Production Room', 'ตู้เก็บของ 3'),
	(2, 'ENG ROOM', ''),
	(3, 'Main ware House', NULL),
	(4, 'Stock', NULL),
	(5, 'IM Room', NULL),
	(6, 'Main Warehouse ', '	ZCOM'),
	(7, 'บ้านสร้าง', 'ปราจีนบุรี');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;

-- Dumping structure for table inventory.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) NOT NULL,
  `action_name` varchar(255) NOT NULL COMMENT 'create/update/delete',
  `created_by` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.log: ~0 rows (approximately)
DELETE FROM `log`;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;

-- Dumping structure for table inventory.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.menu: ~0 rows (approximately)
DELETE FROM `menu`;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Dumping structure for table inventory.migration
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.migration: ~19 rows (approximately)
DELETE FROM `migration`;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1600064638),
	('m140209_132017_init', 1600064641),
	('m140403_174025_create_account_table', 1600064641),
	('m140504_113157_update_tables', 1600064641),
	('m140504_130429_create_token_table', 1600064641),
	('m140506_102106_rbac_init', 1600077816),
	('m140602_111327_create_menu_table', 1600065332),
	('m140830_171933_fix_ip_field', 1600064641),
	('m140830_172703_change_account_table_name', 1600064641),
	('m141222_110026_update_ip_field', 1600064641),
	('m141222_135246_alter_username_length', 1600064641),
	('m150614_103145_update_social_account_table', 1600064642),
	('m150623_212711_fix_username_notnull', 1600064642),
	('m151218_234654_add_timezone_to_profile', 1600064642),
	('m160312_050000_create_user', 1600065332),
	('m160929_103127_add_last_login_at_to_user_table', 1600064642),
	('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1600077816),
	('m180523_151638_rbac_updates_indexes_without_prefix', 1600077816),
	('m200409_110543_rbac_update_mssql_trigger', 1600077816);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;

-- Dumping structure for table inventory.profile
CREATE TABLE IF NOT EXISTS `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table inventory.profile: ~3 rows (approximately)
DELETE FROM `profile`;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`, `timezone`) VALUES
	(1, 'Administrator', 'admin@umtex.com', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;

-- Dumping structure for table inventory.project
CREATE TABLE IF NOT EXISTS `project` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_no` varchar(32) DEFAULT NULL,
  `pro_name` varchar(255) NOT NULL,
  `project_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pro_id`) USING BTREE,
  UNIQUE KEY `project_no` (`project_no`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.project: ~0 rows (approximately)
DELETE FROM `project`;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` (`pro_id`, `project_no`, `pro_name`, `project_desc`) VALUES
	(1, 'DMJ-500', 'Manpack Jammer', ''),
	(2, 'UMT-0002', 'Flex Radio', ''),
	(3, 'UMT-0003', 'Antidrone', 'Demo ADS');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;

-- Dumping structure for table inventory.retrieve
CREATE TABLE IF NOT EXISTS `retrieve` (
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `description` text,
  `retrieve_date` date NOT NULL,
  `retrieve_name` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  KEY `FK_retrieve_project` (`project_id`),
  KEY `FK_retrieve_item` (`item_id`),
  KEY `FK_retrieve_location` (`location_id`),
  CONSTRAINT `FK_retrieve_item` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_retrieve_location` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_retrieve_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.retrieve: ~4 rows (approximately)
DELETE FROM `retrieve`;
/*!40000 ALTER TABLE `retrieve` DISABLE KEYS */;
INSERT INTO `retrieve` (`item_id`, `location_id`, `project_id`, `qty`, `description`, `retrieve_date`, `retrieve_name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(4, 2, 1, 500, NULL, '2020-09-27', 'Yutthanan Pipan', 1598237435, 1, NULL, NULL),
	(9, 3, 1, 100, NULL, '2020-09-27', 'Yutthanan Pipan', 1598237435, 1, NULL, NULL),
	(2, 2, 1, 100, NULL, '2020-03-27', 'Yutthanan Pipan', 1585369175, 1, NULL, NULL),
	(2, 3, 2, 10, NULL, '2020-01-27', 'Yutthanan Pipan', 1593317975, NULL, NULL, NULL),
	(20, 7, 1, 10, '', '2020-10-02', 'ยุทธนันท์ พิพันธ์', 1601544368, 1, 1601544368, 1),
	(21, 1, 1, 1, '', '2020-10-01', 'ยุทธนันท์ พิพันธ์', 1601547847, 2, 1601547847, 2);
/*!40000 ALTER TABLE `retrieve` ENABLE KEYS */;

-- Dumping structure for table inventory.social_account
CREATE TABLE IF NOT EXISTS `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  UNIQUE KEY `account_unique_code` (`code`),
  KEY `fk_user_account` (`user_id`),
  CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table inventory.social_account: ~0 rows (approximately)
DELETE FROM `social_account`;
/*!40000 ALTER TABLE `social_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_account` ENABLE KEYS */;

-- Dumping structure for table inventory.sub_category
CREATE TABLE IF NOT EXISTS `sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sub_category_category` (`category_id`) USING BTREE,
  CONSTRAINT `FK_sub_category_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.sub_category: ~32 rows (approximately)
DELETE FROM `sub_category`;
/*!40000 ALTER TABLE `sub_category` DISABLE KEYS */;
INSERT INTO `sub_category` (`id`, `name`, `category_id`) VALUES
	(8, 'Charger', 5),
	(9, 'อิเล็กโตไลน์', 6),
	(10, 'เซรามิค', 6),
	(11, 'WIMA', 6),
	(12, 'TK-7100', 7),
	(13, 'TK-90', 7),
	(14, 'SMD', 8),
	(15, 'SAW', 10),
	(16, 'MiniCircuit', 10),
	(18, 'Jammer', 16),
	(19, 'Radio', 16),
	(20, '100W', 11),
	(21, '200W', 11),
	(22, 'AC to DC', 3),
	(23, 'DC to DC', 3),
	(24, 'Inverter', 3),
	(25, 'a', 15),
	(26, 'b', 15),
	(28, 'DIP (1/4 W)', 2),
	(29, '0805 (1/8 W)', 2),
	(30, '2010 (1/2 W)', 2),
	(31, '2512 (1 W)', 2),
	(32, '1210 (1/3 W)', 2),
	(33, '448C', 2),
	(34, '450C', 2),
	(35, '449C', 2),
	(66, 'BlankPlate', 1),
	(67, '2U Rack', 1),
	(68, '3U Rack', 1),
	(69, 'TK-7100', 1),
	(70, 'TK-90', 1),
	(87, '	Mini-Circuit', 4);
/*!40000 ALTER TABLE `sub_category` ENABLE KEYS */;

-- Dumping structure for table inventory.supplier
CREATE TABLE IF NOT EXISTS `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(255) NOT NULL,
  `contact_persion` varchar(255) DEFAULT NULL,
  `contact_firstname` varchar(255) DEFAULT NULL,
  `address` text,
  `phone_primary` varchar(15) DEFAULT NULL,
  `phone_alternative` varchar(15) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `supplier_name` (`supplier_name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.supplier: ~12 rows (approximately)
DELETE FROM `supplier`;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` (`id`, `supplier_name`, `contact_persion`, `contact_firstname`, `address`, `phone_primary`, `phone_alternative`, `fax`, `email`) VALUES
	(1, 'AMP Metal Work', '', '', '', '', '', '', ''),
	(2, 'DRC', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Electronics Sources Co.,Ltd.', '', '', '', '', '', '', ''),
	(4, 'Heng Fu', 'Frank', NULL, NULL, NULL, NULL, NULL, 'overseas@hengfupower.com'),
	(5, 'KPT', 'TAL', 'patcharawan', '', '', '', '', ''),
	(6, 'MITSUBISHI', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'Mouser', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 'RF CORE', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'RS Thailand', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 'Siam Industrial', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'Spark', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 'Thai Sen', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 'Yangzhu', 'Kevin Kong', NULL, NULL, NULL, NULL, NULL, 'jingcheng0514@aliyun.com');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;

-- Dumping structure for table inventory.token
CREATE TABLE IF NOT EXISTS `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`),
  CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table inventory.token: ~0 rows (approximately)
DELETE FROM `token`;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token` ENABLE KEYS */;

-- Dumping structure for table inventory.uploads
CREATE TABLE IF NOT EXISTS `uploads` (
  `upload_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` varchar(50) DEFAULT NULL,
  `file_name` varchar(150) DEFAULT NULL COMMENT 'ชื่อไฟล์',
  `real_filename` varchar(150) DEFAULT NULL COMMENT 'ชื่อไฟล์จริง',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`upload_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- Dumping data for table inventory.uploads: ~10 rows (approximately)
DELETE FROM `uploads`;
/*!40000 ALTER TABLE `uploads` DISABLE KEYS */;
INSERT INTO `uploads` (`upload_id`, `ref`, `file_name`, `real_filename`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(34, 'wuEdfWgTUQ5-EW6mF3WnXm', 'steel.jpg', '52dde6b0f919f4031357cd6133c63440.jpg', 1601543651, 1, 1601543651, 1),
	(35, 'RbynO3e_HXUMuYDVr6l2nQ', 'steel.jpg', 'a96d34262ee9d9acb09245c6a77642a9.jpg', 1601547696, 2, 1601547696, 2);
/*!40000 ALTER TABLE `uploads` ENABLE KEYS */;

-- Dumping structure for table inventory.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_username` (`username`),
  UNIQUE KEY `user_unique_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table inventory.user: ~3 rows (approximately)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`) VALUES
	(1, 'admin', 'sam.rakchart@gmail.com', '$2y$12$giscjizOxAu./WwcxiDjReEKSin2vMlp./tKdDj4NGIdQNHPqgENa', 'aCCxDuBACA36lMshpJ_6N78Hk5Bp--Yp', 1600076997, NULL, NULL, '::1', 1600076933, 1601607966, 0, 1601607942),
	(2, 'user1', 'user1@gmail.com', '$2y$12$SYEbHl3IXVoPlf6UKFWE2OBKFJq5AX0RTAdBaXGdkuMlYdMJZGvde', '6y3kcqJ_LD1VeZ0vXxTM9ASqGh5ooxxV', 1600233518, NULL, NULL, '::1', 1600233518, 1600233518, 0, 1601547578),
	(3, 'viewer', 'viewer@umtex.com', '$2y$12$l6TY.zvu9d6msWs0dwjCZeSt4nulyDsMZKVAsXoF6N9o75XLWphGS', '5nHNOYIWsdVptQb9YzLdn104B_X75zOZ', 1600242081, NULL, NULL, '::1', 1600242081, 1601534889, 0, 1601541184),
	(4, 'yutthapong@umtex.com', 'yutthapong@umtex.com', '$2y$12$sT1YmlcFyBOUJSEShV6fwOgK5FMmiwOYHO/ZGvbZi8cKQKcvGdW3C', 'WRd8-y0ccNWg1vvZZsWjh_3djRVScwWW', 1601602254, NULL, NULL, '::1', 1601602254, 1601602254, 0, 1601602597),
	(5, 'sam', 'yutthanan@umtex.com', '$2y$12$4/F/R2ng/IW5K7gC2mZtoeoqCnYAmMOewlc6u1oD/1qwroaVi/hBK', '-EquMiA0lakPXN3lW74qgp8u-v_e9_Kp', 1601608006, NULL, NULL, '172.16.10.18', 1601608006, 1601608006, 0, 1601608103);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
