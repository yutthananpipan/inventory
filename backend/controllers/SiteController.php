<?php

namespace backend\controllers;

use common\models\Category;
use common\models\Item;
use common\models\Location;
use common\models\Project;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $item = Item::find()->count();
        $project = Project::find()->count();
        $category = Category::find()->count();
        $location = Location::find()->count();

        return $this->render('@app/views/site/dashboard.php', [
            'item' => $item,
            'project' => $project,
            'category' => $category,
            'location' => $location,
            'category_graph' => $this->getCategoryGraph(),
            'location_graph' => $this->getLocationGraph(),
            'retrieve_graph' => $this->getRetrieveGraph(),
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    private function getCategoryGraph()
    {
        $color = [
            '#e83e8c', '#dc3545', '#fd7e14', '#ffc107', '#28a745',
            '#C70039', '#900C3F', '#581845', '#C0C0C0', '#808080',
            '#000000', '#FF0000', '	#800000', '#FFFF00', '#808000',
            '#00FF00', '#008000', '#00FFFF', '#008080', '#800080',
            '#fd7e14', '#ffc107', '#DAF7A6', '#FFC300', '#FF5733',
        ];
        $category_query = Category::find()
            ->select(['COUNT(*) AS cnt', 'category.name'])
            ->join('INNER JOIN', 'item', 'category.id = item.category_id')
            ->groupBy(['category.id'])
            ->orderBy(['category.name' => SORT_ASC])->all();

        $category_graph = null;
        $index = 0;

        foreach ($category_query as $i) {
            $category_graph[] = [
                'name' => $i->name,
                'y' => (int)$i->cnt,
                'color' => $color[$index % count($color)]
            ];
            $index++;
        }

        return $category_graph;
    }

    private function getLocationGraph()
    {
        $color = [
            '#28a745', '#20c997', '#17a2b8', '#e83e8c', '#dc3545',
            '#fd7e14', '#ffc107', '#DAF7A6', '#FFC300', '#FF5733',
            '#C70039', '#900C3F', '#581845', '#C0C0C0', '#808080',
            '#000000', '#FF0000', '	#800000', '#FFFF00', '#808000',
            '#00FF00', '#008000', '#00FFFF', '#008080', '#800080'
        ];

        $sql = 'SELECT loc.id, loc.name, loc.address, c2.cnt FROM location loc
        INNER JOIN (SELECT COUNT(inv.location_id) AS cnt, inv.location_id AS loc_id FROM inventory inv 
        INNER JOIN item it ON inv.item_id = it.id GROUP BY inv.location_id) c2 
        ON loc.id = c2.loc_id';

        $rows = Yii::$app->db->createCommand($sql)->queryAll();

        $location_graph = null;
        $index = 0;

        foreach ($rows as $i) {
            $location_graph[] = [
                'name' => $i['name'],
                'y' => (int)$i['cnt'],
                'color' => $color[$index % count($color)]
            ];
            $index++;
        }

        return $location_graph;
    }

    private function getRetrieveGraph()
    {
        $retrieve_graph = null;
        $sql = 'SELECT * FROM project p INNER JOIN	(SELECT ret.project_id, COUNT(*) as cnt, 
        COUNT(IF(MONTH((ret.retrieve_date))=1,ret.project_id,NULL)) AS m1,
        COUNT(IF(MONTH((ret.retrieve_date))=2,ret.project_id,NULL)) AS m2,
        COUNT(IF(MONTH((ret.retrieve_date))=3,ret.project_id,NULL)) AS m3,
        COUNT(IF(MONTH((ret.retrieve_date))=4,ret.project_id,NULL)) AS m4,
        COUNT(IF(MONTH((ret.retrieve_date))=5,ret.project_id,NULL)) AS m5,
        COUNT(IF(MONTH((ret.retrieve_date))=6,ret.project_id,NULL)) AS m6,
        COUNT(IF(MONTH((ret.retrieve_date))=7,ret.project_id,NULL)) AS m7,
        COUNT(IF(MONTH((ret.retrieve_date))=8,ret.project_id,NULL)) AS m8,
        COUNT(IF(MONTH((ret.retrieve_date))=9,ret.project_id,NULL)) AS m9,
        COUNT(IF(MONTH((ret.retrieve_date))=10,ret.project_id,NULL)) AS m10,
        COUNT(IF(MONTH((ret.retrieve_date))=11,ret.project_id,NULL)) AS m11,
        COUNT(IF(MONTH((ret.retrieve_date))=12,ret.project_id,NULL)) AS m12
        FROM retrieve ret WHERE YEAR(NOW())=YEAR((ret.retrieve_date))
        GROUP BY ret.project_id) c2 ON c2.project_id = p.pro_id';

        $rows = Yii::$app->db->createCommand($sql)->queryAll();

        $retrieve_graph = null;

        foreach ($rows as $model) {
            $retrieve_graph[] = [
                'type' => 'line',
                'name' => $model['pro_name'],
                'data' => [
                    ['Jan', intval($model['m1'])],
                    ['Feb', intval($model['m2'])],
                    ['Mar', intval($model['m3'])],
                    ['Apr', intval($model['m4'])],
                    ['May', intval($model['m5'])],
                    ['June', intval($model['m6'])],
                    ['Jul', intval($model['m7'])],
                    ['Aug', intval($model['m8'])],
                    ['Sep', intval($model['m9'])],
                    ['Oct', intval($model['m10'])],
                    ['Nov', intval($model['m11'])],
                    ['Dec', intval($model['m12'])]]
            ];
        }
        return $retrieve_graph;
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
