<?php

namespace backend\controllers;

use common\models\SubCategory;
use phpDocumentor\Reflection\Types\Object_;
use Yii;
use common\models\Category;
use common\models\CategorySearch;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelCategory = new Category();
        $modelSubCategorys = json_encode(array());

        if ($modelCategory->load(Yii::$app->request->post()) && $modelCategory->save()) {
            $s = Yii::$app->request->post('sub_category');
            if ($s) {
                foreach ($s as $item) {
                    if(!empty($item)){
                        $sub_cat = new SubCategory();
                        $sub_cat->name = $item;
                        $sub_cat->category_id = $modelCategory->id;
                        $sub_cat->save();
                    }
                }
            }

            \Yii::$app->getSession()->setFlash('success', 'Category have been created.');
            return $this->redirect(['view', 'id' => $modelCategory->id]);
        }

        return $this->render('create', [
            'modelCategory' => $modelCategory,
            'modelSubCategorys' => $modelSubCategorys,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        Url::remember('', 'actions-redirect');
        $modelCategory = $this->findModel($id);
        $modelSubCategorys = SubCategory::find()->where(['category_id' => $modelCategory->id])->all();

        $index = 0;
        $json_arrays = array();
        foreach ($modelSubCategorys as $m) {
            $obj = new Object_();
            $obj->id = $m->id;
            $obj->name = $m->name;
            $json_arrays[$index] = $obj;
            $index++;
        }

        $modelSubCategorys = json_encode($json_arrays);

        if ($modelCategory->load(Yii::$app->request->post())) {

            SubCategory::deleteAll(['category_id' => $modelCategory->id]);

            $s = Yii::$app->request->post('sub_category');
            if ($s) {
                foreach ($s as $item) {
                    if(!empty($item)){
                        $sub_cat = new SubCategory();
                        $sub_cat->name = $item;
                        $sub_cat->category_id = $modelCategory->id;
                        $sub_cat->save();
                    }
                }
            }

            $modelCategory->save();

            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Category have been updated'));

            return $this->redirect(['view', 'id' => $modelCategory->id]);
        }

        return $this->render('update', [
            'modelCategory' => $modelCategory,
            'modelSubCategorys' => $modelSubCategorys,
            'sub_category' => [],
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAddSubCategory($id)
    {
//        $model = new SubCategory();
//        $model->save();
        return $this->redirect(['update', 'id' => $id]);
    }

    public function actionRemoveSubCategory($id)
    {
        //SubCategory::findOne($id)->delete();
        return $this->redirect(['update', 'id' => $id]);
    }

    // THE CONTROLLER
    public function actionSubcat()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                if ($cat_id != null) {
                    $subs = SubCategory::find()->where(['category_id' => $cat_id])->all();
                    foreach ($subs as $s) {
                        $o['id'] = $s->id;
                        $o['name'] = $s->name;

                        $out[] = $o;
                    }

                    return ['output' => $out, 'selected' => ''];
                }
            }
        }
        return ['output' => $out, 'selected' => ''];
    }

}
