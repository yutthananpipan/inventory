<?php

namespace backend\controllers;

use common\models\Inventory;
use common\models\Location;
use common\models\Retrieve;
use common\models\Uploads;
use phpDocumentor\Reflection\Types\Object_;
use Yii;
use common\models\Item;
use common\models\ItemSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseFileHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemSearch();

        $itemsearch = Yii::$app->request->post('itemsearch');
        $dataProvider = ($itemsearch == null) ? $searchModel->search(Yii::$app->request->queryParams) : $searchModel->searchitem($itemsearch);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRetrieve($id)
    {
        $modelItem = $this->findModel($id);
        $modelRetrieve = new Retrieve();

        $locations = [];
        $loc_q = Inventory::find()
            ->select(['inventory.item_id', 'inventory.location_id', 'location.name', 'inventory.qty', 'inventory.ideal_qty', 'inventory.warn_qty'])
            ->join('INNER JOIN', 'location', 'inventory.location_id = location.id and inventory.qty > 0')
            ->where(['item_id' => $modelItem->id])
            ->orderBy(['created_at' => SORT_ASC])
            ->all();

        foreach ($loc_q as $l) {
            $locations[$l->location->id] = $l->location->name . ' (' . $l->qty . ')';
        }

        $modelInventory = Inventory::find()
            ->select(['SUM(qty) AS sum_qty', 'SUM(ideal_qty) AS sum_ideal_qty', 'SUM(warn_qty) AS sum_warn_qty', 'item_id'])
            ->where(['item_id' => $modelItem->id])
            ->groupBy(['item_id'])
            ->one();

        $obj = new Object_();
        $obj->sum_qty = ($modelInventory == null) ? 0 : (($modelInventory->sum_qty == 0) ? 0 : $modelInventory->sum_qty);
        $obj->sum_ideal_qty = ($modelInventory == null) ? 0 : (($modelInventory->sum_ideal_qty == 0) ? 0 : $modelInventory->sum_ideal_qty);
        $obj->sum_warn_qty = ($modelInventory == null) ? 0 : (($modelInventory->sum_warn_qty == 0) ? 0 : $modelInventory->sum_warn_qty);
        $modelInventory = json_encode($obj);

        if ($modelRetrieve->load(Yii::$app->request->post())) {
            $inv = Inventory::find()->where(['item_id' => $modelItem->id, 'location_id' => $modelRetrieve->location_id])
                ->one();
            if ($inv) {
                if (($modelRetrieve->qty <= $inv->qty) && $modelRetrieve->qty > 0) {
                    $inv->qty = $inv->qty - $modelRetrieve->qty;

                    $modelRetrieve->item_id = $modelItem->id;

                    if ($modelRetrieve->save()) {
                        $inv->save();

                        \Yii::$app->getSession()->setFlash('success', 'Retrieve have been created.');

                        return $this->redirect(['item/retrieve-report']);
                    } else {
                        \Yii::$app->getSession()->setFlash('danger', 'Retrieve not success please check description');
                    }
                } else {
                    \Yii::$app->getSession()->setFlash('danger', 'Not enough items in this location');

                    return $this->render('retrieve', [
                        'modelItem' => $modelItem,
                        'modelRetrieve' => $modelRetrieve,
                        'modelInventory' => $modelInventory,
                        'locations' => $locations,

                    ]);
                }
            }

        }

        return $this->render('retrieve', [
            'modelItem' => $modelItem,
            'modelRetrieve' => $modelRetrieve,
            'modelInventory' => $modelInventory,
            'locations' => $locations,

        ]);
    }

    public function actionRetrieves()
    {
        $modelRetrieve = new Retrieve();

        $items = [];
        $loc_q = Inventory::find()
            ->select(['inventory.item_id', 'inventory.location_id', 'item.name', 'location.name', 'inventory.qty'])
            ->join('INNER JOIN', 'location', 'inventory.location_id = location.id and inventory.qty > 0')
            ->join('INNER JOIN', 'item', 'item.id = inventory.item_id')
            ->orderBy(['item.name' => SORT_ASC])
            ->all();

        foreach ($loc_q as $l) {
            $items[] = $l->item->name . '------>' . $l->location->name . ' [' . $l->qty . ']';
        }

        if ($modelRetrieve->load(Yii::$app->request->post()) && $modelRetrieve->validate(['project_id', 'retrieve_name', 'retrieve_date'])) {

            $item_select = $modelRetrieve->item_id;
            $qty_select = $modelRetrieve->qty;
            for ($in = 0; $in < count($item_select); $in++) {
                $it = $item_select[$in];
                $qt = $qty_select[$in];
                if (trim($it) != "" && trim($qt) != "") {
                    $ret = new Retrieve();
                    $ret->item_id = $loc_q[$it]->item_id;
                    $ret->location_id = $loc_q[$it]->location_id;
                    $ret->qty = $qt;
                    $ret->project_id = $modelRetrieve->project_id;
                    $ret->retrieve_date = $modelRetrieve->retrieve_date;
                    $ret->retrieve_name = $modelRetrieve->retrieve_name;
                    $ret->description = $modelRetrieve->description;

                    $inv = Inventory::find()->where(['item_id' => $ret->item_id, 'location_id' => $ret->location_id])
                        ->one();
                    if ($inv) {
                        if (($ret->qty <= $inv->qty) && $ret->qty > 0) {
                            $inv->qty = $inv->qty - $ret->qty;

                            if ($ret->save()) {
                                $inv->save();

                                \Yii::$app->getSession()->setFlash('success', $loc_q[$it]->item->name . ' have been retrieved.');

                                if (count($item_select) - 1 == $in) {
                                    return $this->redirect(['item/retrieve-report']);
                                }

                            } else {
                                \Yii::$app->getSession()->setFlash('danger', $loc_q[$it]->item->name . ' can not retrieve please check description');
                            }
                        } else {
                            \Yii::$app->getSession()->setFlash('danger', $loc_q[$it]->item->name . ' not enough in this location');

                        }
                    }

                } else {
                    \Yii::$app->getSession()->setFlash('warning', 'Please insert item or qty');
                }
            }
        }

        return $this->render('retrieves', [
            'items' => $items,
            'modelRetrieve' => $modelRetrieve,
        ]);
    }

    public function actionRetrieveReport()
    {
        $retrieveModel = Retrieve::find()->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $retrieveModel,
        ]);

        return $this->render('retrieve_report', [
            'dataProvider' => $dataProvider,
            'retrieveModel' => $retrieveModel,

        ]);
    }

    public function actionRetrieveLocation()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        /*if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                if ($cat_id != null) {
                    $subs = SubCategory::find()->where(['category_id' => $cat_id])->all();
                    foreach ($subs as $s) {
                        $o['id'] = $s->id;
                        $o['name'] = $s->name;

                        $out[] = $o;
                    }

                    return ['output' => $out, 'selected' => ''];
                }
            }
        }*/
        return ['output' => $out, 'selected' => ''];
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $modelInventory = Inventory::find()
            ->select(['inventory.item_id', 'inventory.location_id', 'location.name', 'inventory.qty', 'inventory.ideal_qty', 'inventory.warn_qty'])
            ->join('INNER JOIN', 'location', 'inventory.location_id = location.id and inventory.qty > 0')
            ->where(['item_id' => $id])
            ->orderBy(['created_at' => SORT_ASC])
            ->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelInventory' => $modelInventory,
        ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelItem = new Item();
        $modelInventory = json_encode(array());

        if ($modelItem->load(Yii::$app->request->post()) && $modelItem->save()) {
            $this->Uploads(false);

            $amount = 0;

            $qty = Yii::$app->request->post('qty');
            $idel_qty = Yii::$app->request->post('idel_qty');
            $warn_qty = Yii::$app->request->post('warn_qty');

            $loc_name = Yii::$app->request->post('name');
            if ($loc_name) {

                $i = 0;
                foreach ($loc_name as $ln) {
                    if (!empty($ln)) {
                        $model_loc = Location::find()->where(['name' => $ln])->one();

                        $inv = new Inventory();
                        $inv->item_id = $modelItem->id;
                        $inv->location_id = $model_loc->id;
                        $inv->qty = (empty($qty[$i]) ? 0 : $qty[$i]);
                        $inv->ideal_qty = (empty($idel_qty[$i]) ? 0 : $idel_qty[$i]);
                        $inv->warn_qty = (empty($warn_qty[$i]) ? 0 : $warn_qty[$i]);

                        $amount = $amount + $inv->qty;

                        $inv->save();

                        $i++;
                    }
                }
            }

            $modelItem->qty = $amount;
            $modelItem->save();

            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Item have been created'));
            return $this->redirect(['view', 'id' => $modelItem->id]);
        } else {
            $modelItem->ref = substr(Yii::$app->getSecurity()->generateRandomString(), 10);
        }

        return $this->render('create', [
            'modelItem' => $modelItem,
            'modelInventory' => $modelInventory,

        ]);
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $modelItem = $this->findModel($id);
        $modelInventory = Inventory::find()
            ->select(['inventory.item_id', 'inventory.location_id', 'location.name', 'inventory.qty', 'inventory.ideal_qty', 'inventory.warn_qty'])
            ->join('INNER JOIN', 'location', 'inventory.location_id = location.id and inventory.qty > 0')
            ->where(['item_id' => $modelItem->id])
            ->orderBy(['created_at' => SORT_ASC])
            ->all();

        $index = 0;
        $json_arrays = array();
        foreach ($modelInventory as $m) {
            $obj = new Object_();
            $obj->location_id = $m->location_id;
            $obj->name = $m->location->name;
            $obj->qty = ($m->qty == 0) ? 0 : $m->qty;
            $obj->ideal_qty = ($m->ideal_qty == 0) ? 0 : $m->ideal_qty;
            $obj->warn_qty = ($m->warn_qty == 0) ? 0 : $m->warn_qty;
            $json_arrays[$index] = $obj;
            $index++;
        }

        $modelInventory = json_encode($json_arrays);

        list($initialPreview, $initialPreviewConfig) = $this->getInitialPreview($modelItem->ref);

        if ($modelItem->load(Yii::$app->request->post())) {

            $this->Uploads(false);

            $qty = Yii::$app->request->post('qty');
            $idel_qty = Yii::$app->request->post('idel_qty');
            $warn_qty = Yii::$app->request->post('warn_qty');

            $loc_name = Yii::$app->request->post('name');
            if ($loc_name) {
                Inventory::deleteAll(['item_id' => $modelItem->id]);
                $i = 0;
                foreach ($loc_name as $ln) {
                    if (!empty($ln)) {
                        $model_loc = Location::find()->where(['name' => $ln])->one();

                        $inv = new Inventory();
                        $inv->item_id = $modelItem->id;
                        $inv->location_id = $model_loc->id;
                        $inv->qty = (empty($qty[$i]) ? 0 : $qty[$i]);
                        $inv->ideal_qty = (empty($idel_qty[$i]) ? 0 : $idel_qty[$i]);
                        $inv->warn_qty = (empty($warn_qty[$i]) ? 0 : $warn_qty[$i]);
                        $inv->save();

                        $i++;
                    }
                }
            } else {
                Inventory::deleteAll(['item_id' => $modelItem->id]);
            }

            $modelItem->save();

            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Item have been updated'));
            return $this->redirect(['view', 'id' => $modelItem->id]);
        }

        return $this->render('update', [
            'modelItem' => $modelItem,
            'modelInventory' => $modelInventory,
            'initialPreview' => $initialPreview,
            'initialPreviewConfig' => $initialPreviewConfig,
        ]);
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        //remove upload file & data
        $this->removeUploadDir($model->ref);
        Uploads::deleteAll(['ref' => $model->ref]);

        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /*|*********************************************************************************|
    |================================ Upload Ajax ====================================|
    |*********************************************************************************|*/

    public function actionUploadAjax()
    {
        $this->Uploads(true);
    }

    private function CreateDir($folderName)
    {
        if ($folderName != NULL) {
            $basePath = Item::getUploadPath();
            if (BaseFileHelper::createDirectory($basePath . $folderName, 0777)) {
                BaseFileHelper::createDirectory($basePath . $folderName . '/thumbnail', 0777);
            }
        }
        return;
    }

    private function removeUploadDir($dir)
    {
        BaseFileHelper::removeDirectory(Item::getUploadPath() . $dir);
    }

    private function Uploads($isAjax = false)
    {
        if (Yii::$app->request->isPost) {

            $images = UploadedFile::getInstancesByName('upload_ajax');
            if ($images) {

                if ($isAjax === true) {
                    $ref = Yii::$app->request->post('ref');
                } else {
                    $item = Yii::$app->request->post('Item');
                    $ref = $item['ref'];

                }

                $this->CreateDir($ref);

                foreach ($images as $file) {
                    $fileName = $file->baseName . '.' . $file->extension;
                    $realFileName = md5($file->baseName . time()) . '.' . $file->extension;
                    $savePath = Item::UPLOAD_FOLDER . '/' . $ref . '/' . $realFileName;
                    if ($file->saveAs($savePath)) {

                        $file_path = Item::getUploadPath() . $ref . '/' . $realFileName;
                        if ($this->isImage($file_path)) {
                            $this->createThumbnail($ref, $realFileName, 250);
                        }

                        $model = new Uploads;
                        $model->ref = $ref;
                        $model->file_name = $fileName;
                        $model->real_filename = $realFileName;
                        $model->save();

                        if ($isAjax === true) {
                            echo json_encode(['success' => 'true']);
                        }

                    } else {
                        if ($isAjax === true) {
                            echo json_encode(['success' => 'false', 'eror' => $file->error]);
                        }
                    }

                }
            }
        }
    }

    private function getInitialPreview($ref)
    {
        $datas = Uploads::find()->where(['ref' => $ref])->all();
        $initialIconPreview = [];
        $initialIconPreviewConfig = [];
        foreach ($datas as $key => $value) {
            array_push($initialIconPreview, $this->getTemplatePreview($value));
            array_push($initialIconPreviewConfig, [
                'caption' => $value->file_name,
                'width' => '260px',
                'url' => Url::toRoute(['item/deletefile-ajax']),
                'key' => $value->upload_id
            ]);
        }
        return [$initialIconPreview, $initialIconPreviewConfig];
    }

    public function isImage($filePath)
    {
        return @is_array(getimagesize($filePath)) ? true : false;
    }

    private function getTemplatePreview(Uploads $model)
    {
        $file_path = Item::getUploadPath() . $model->ref . '/' . $model->real_filename;
        $isImage = $this->isImage($file_path);
        if ($isImage) {
            $file = Html::img(Item::getUploadUrl() . $model->ref . '/' . $model->real_filename, ['class' => 'img-fluid', 'alt' => $model->file_name, 'title' => $model->file_name]);
        } else {
            $file = "<div class='img-fluid'> " .
                "<h2><i class='	fa fa-file-picture-o text-danger'></i></h2>" .
                "</div>";
        }
        return $file;
    }

    private function createThumbnail($ref, $fileName, $width = 250)
    {
        $uploadPath = Item::getUploadPath() . $ref . '/';
        $file = $uploadPath . $fileName;
        $image = Yii::$app->image->load($file);
        $image->resize($width);
        $image->save($uploadPath . 'thumbnail/' . $fileName);
        return;
    }

    public function actionDeletefileAjax()
    {
        $model = Uploads::findOne(Yii::$app->request->post('key'));
        if ($model !== NULL) {
            $filename = Item::getUploadPath() . $model->ref . '/' . $model->real_filename;
            $thumbnail = Item::getUploadPath() . $model->ref . '/thumbnail/' . $model->real_filename;
            if ($model->delete()) {
                @unlink($filename);
                @unlink($thumbnail);
            }
            echo json_encode(['success' => true]);
        } else {
            echo json_encode(['success' => false]);
        }
    }
}
