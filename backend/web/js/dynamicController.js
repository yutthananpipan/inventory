var app = angular.module('dynamicApp', []);

console.log('angularjs started');

app.controller('dynamicController', function ($scope) {
    $scope.rows = [];

    $scope.modelSubCategorys = [];

    $scope.addRow = function () {
        var id = $scope.rows.length + 1;
        $scope.rows.push({'id': 'dynamic' + id, 'qty': 0, 'ideal_qty': 0, 'warn_qty': 0});
    };

    $scope.removeRow = function (index) {
        $scope.rows.splice(index, 1);
    };

    $scope.formData = {};

});