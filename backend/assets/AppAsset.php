<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //  <!-- Font Awesome -->
        'AdminLTE-master/plugins/fontawesome-free/css/all.min.css',
        //  <!-- Tempusdominus Bbootstrap 4 -->
        'AdminLTE-master/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css',
        //  <!-- iCheck -->
        'AdminLTE-master/plugins/icheck-bootstrap/icheck-bootstrap.min.css',
        //  <!-- JQVMap -->
        'AdminLTE-master/plugins/jqvmap/jqvmap.min.css',
        //  <!-- Theme style -->
        'AdminLTE-master/dist/css/adminlte.min.css',
        //  <!-- overlayScrollbars -->
        'AdminLTE-master/plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
        //  <!-- Daterange picker -->
        'AdminLTE-master/plugins/daterangepicker/daterangepicker.css',
        //  <!-- summernote -->
        'AdminLTE-master/plugins/summernote/summernote-bs4.css',
        'css/inventory.css',
    ];
    public $js = [
        //<!-- jQuery UI 1.11.4 -->
        'AdminLTE-master/plugins/jquery-ui/jquery-ui.min.js',
        //<!-- Bootstrap 4 -->
        'AdminLTE-master/plugins/bootstrap/js/bootstrap.bundle.min.js',
        //<!-- ChartJS -->
        'AdminLTE-master/plugins/chart.js/Chart.min.js',
        //<!-- Sparkline -->
        'AdminLTE-master/plugins/sparklines/sparkline.js',
        //<!-- JQVMap -->
        'AdminLTE-master/plugins/jqvmap/jquery.vmap.min.js',
        'AdminLTE-master/plugins/jqvmap/maps/jquery.vmap.usa.js',
        //<!-- jQuery Knob Chart -->
        'AdminLTE-master/plugins/jquery-knob/jquery.knob.min.js',
        //<!-- daterangepicker -->
        'AdminLTE-master/plugins/moment/moment.min.js',
        'AdminLTE-master/plugins/daterangepicker/daterangepicker.js',
        //<!-- Tempusdominus Bootstrap 4 -->
        'AdminLTE-master/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js',
        //<!-- Summernote -->
        'AdminLTE-master/plugins/summernote/summernote-bs4.min.js',
        //<!-- overlayScrollbars -->
        'AdminLTE-master/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
        //<!-- AdminLTE App -->
        'AdminLTE-master/dist/js/adminlte.js',
        //<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        'AdminLTE-master/dist/js/pages/dashboard.js',
        //<!-- AdminLTE for demo purposes -->
        'AdminLTE-master/dist/js/demo.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
