<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Location */

$this->title = 'Create Location';
$this->params['breadcrumbs'][] = ['label' => 'Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="mt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a
                        href="<?php echo Url::toRoute(['location/index']) ?>">Location</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create</li>
        </ol>
    </nav>
</div>

<div class="card card-dark p-0">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <div>
                    <h4 class="m-0 text-light"><i class="fas fa-map-marked mr-1"></i><?= $this->title ?></h4>
                </div><!-- /.col -->
            </div>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
