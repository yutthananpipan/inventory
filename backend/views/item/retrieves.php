<?php

use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $modelRetrieve \common\models\Retrieve ?> */
/* @var $items [] common\models\Item */

$this->title = 'Retrieves';
?>
<div class="mt-3">

    <?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user'),]) ?>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a
                        href="<?php echo Url::toRoute(['item/index']) ?>">Item</a></li>
            <li class="breadcrumb-item active" aria-current="page">Retrieves</li>
        </ol>
    </nav>
</div>

<div class="card card-dark p-0">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <div>
                    <h4 class="m-0 text-light"><i class="fas fa-cubes mr-1"></i>Retrieves</h4>
                </div><!-- /.col -->
            </div>
        </div>
    </div>

    <div class="card-body">
        <?php $form = ActiveForm::begin(); ?>
        <div ng-app="dynamicApp" ng-controller="dynamicController" ng-init="rows = [{}];">

            <!--            Start Retrieve from Inventory-->
            <div class="card card-dark card-outline">
                <div class="card-header">
                    <h3 class="card-title">
                        Retrieve Item
                    </h3>
                </div>
                <div class="card-body">
                    <div class="form-group pt-2">
                        <div class="row">
                            <div class="col-md-8 col-12"><label>Item</label></div>
                            <div class="col-md-3 col-9"><label>Qty</label></div>
                            <div class="col-md-1 col-3"><label>Remove</label></div>
                        </div>
                    </div>

                    <fieldset ng-repeat="row in rows">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8 col-12">
                                    <?= $form->field($modelRetrieve, 'item_id[]')
                                        ->dropDownList(
                                            $items,
                                            [
                                                'id' => 'item-id{{$index}}',
                                                'prompt' => 'Select Item',
                                            ]
                                        )
                                        ->label(false)
                                    ?>
                                </div>
                                <div class="col-md-3 col-9 pt-1 pt-md-0">
                                    <?= $form->field($modelRetrieve, 'qty[]')->textInput(['maxlength' => true,
                                        'placeholder' => 'Qty', 'ng-model' => 'qty', 'class' => 'form-control'])->label(false)
                                    ?>
                                </div>
                                <div class="col-md-1 col-3 pt-1 pt-md-0">
                                    <div class="pt-1">
                                        <button type="button" name="remove"
                                                class="btn btn-danger btn-sm" ng-click="removeRow($index)"><span
                                                    class="fa fa-minus"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group">
                        <button type="button" name="add_more" class="btn btn-success" ng-click="addRow()"><span
                                    class="fa fa-plus"></span> Add
                        </button>
                    </div>

                </div>
            </div>
            <!--            End Retrieve from Inventory-->

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Project</label>
                <div class="col-sm-10">
                    <?=
                    $form->field($modelRetrieve, 'project_id')->widget(Select2::classname(), [
                        'name' => 'project_id',
                        'hideSearch' => false,
                        'data' => ArrayHelper::map(\common\models\Project::find()->all(), 'pro_id', 'pro_name'),
                        'options' => ['placeholder' => 'Select Project...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Retrieve Date</label>
                <div class="col-sm-10">
                    <?=
                    $form->field($modelRetrieve, 'retrieve_date')->widget(DatePicker::classname(), [
                        'name' => 'receive_date',
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'orientation' => 'top right',
                            'format' => 'yyyy-mm-dd',
                            'autoclose' => true,
                        ]
                    ])->label(false);
                    ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Retrieve Name</label>
                <div class="col-sm-10">
                    <?= $form->field($modelRetrieve, 'retrieve_name')->textInput(['maxlength' => true, 'placeholder' => ''])->label(false) ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                    <?= $form->field($modelRetrieve, 'description')->textarea(['rows' => 3, 'placeholder' => ''])->label(false) ?>
                </div>
            </div>

            <div class="form-group pb-3">
                <div class="row">
                    <div class="col-12">
                        <div class="float-right">
                            <?= Html::submitButton('Retrieves', ['class' => 'btn btn-info']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>


<script src="<?= Url::to('@web/js/angular/angular.min.js') ?>"></script>
<script type="text/javascript" src="<?= Url::to('@web/js/dynamicController.js') ?>"></script>