<?php

use common\models\SubCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $retrieveModel common\models\Retrieve */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Items';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="item-index mt-3">

    <?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user'),]) ?>

    <div class="card card-dark">

        <!--header-->
        <div class="card-header">
            <div class="row">
                <div class="col-12">
                    <div>
                        <h4 class="m-0 text-light"><i class="fas fa-arrow-alt-circle-right mr-1"></i>Retrieve</h4>
                    </div><!-- /.col -->
                </div>
            </div>
        </div>
        <!-- end header-->

        <!--        body-->
        <div class="card-body p-0">

            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'options' => ['class' => 'table-hover table-responsive project'],
                'headerRowOptions' => ['class' => 'text-center'],
                'layout' => "{items}\n{pager}",
                'pager' => [
                    'options' => [
                        'class' => 'pagination  justify-content-center',
                        'style' => ['margin-left' => '15px'],
                    ],

                    'linkContainerOptions' => ['class' => 'page-item'],

                    // Customzing CSS class for pager link
                    'linkOptions' => ['class' => 'page-link'],
                    'activePageCssClass' => 'active',

                    'prevPageCssClass' => 'mypre',
                    'nextPageCssClass' => 'mynext',
                    'firstPageCssClass' => 'myfirst',
                    'lastPageCssClass' => 'mylast',
                ],
                'columns' => [
                    [
                        'header' => 'Item',
                        'value' => 'item.name',
                        'enableSorting' => false,
                    ],
                    [
                        'header' => 'Project',
                        'value' => 'project.pro_name',
                        'enableSorting' => false,
                    ],
                    [
                        'header' => 'Location',
                        'value' => 'location.name',
                        'enableSorting' => false,
                    ],
                    [
                        'header' => 'retrieve qty',
                        'format' => 'html',
                        'value' => function ($model) {
                            return ($model->qty == null) ? '' : '<div class="float-right">' . $model->qty . '</div>';
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'header' => 'stock qty',
                        'format' => 'html',
                        'value' => function ($model) {
                            $inv = \common\models\Inventory::find()
                                ->select(['SUM(qty) AS sum_qty', 'item_id'])
                                ->where(['item_id' => $model->item_id])
                                ->groupBy(['item_id'])
                                ->one();

                            return ($inv == null) ? '0' : '<div class="float-right">' . (($inv->sum_qty == null) ? '0' : $inv->sum_qty) . '</div>';
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'description',
                        'value' => function ($model) {
                            return ($model->description == null) ? '' : $model->description;
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'retrieve_name',
                        'value' => function ($model) {
                            return ($model->retrieve_name == null) ? '' : $model->retrieve_name;
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'retrieve_date',
                        'value' => function ($model) {
                            return ($model->retrieve_date == null) ? '' : $model->retrieve_date;
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'created_at',
                        'value' => function ($model) {
                            if (!$model->created_at || $model->created_at == 0) {
                                return Yii::t('user', 'Never');
                            } else if (extension_loaded('intl')) {
                                return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->created_at]);
                            } else {
                                return date('Y-m-d G:i:s', $model->created_at);
                            }
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'created_by',
                        'value' => function ($model) {
                            $user = \common\models\User::find()->where(['id' => $model->created_by])->one();
                            return ($user == null) ? '' : $user->username;
                        },
                        'enableSorting' => false,
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
        <!--        end body-->
    </div>

</div>
