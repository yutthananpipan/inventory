<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $modelItem common\models\Item */
/* @var $modelInventory [] common\models\Inventory */
/* @var $initialPreview */
/* @var $initialPreviewConfig */

$this->title = 'Update Item: ' . $modelItem->name;
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $modelItem->name, 'url' => ['view', 'id' => $modelItem->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a
                        href="<?php echo Url::toRoute(['item/index']) ?>">Item</a></li>
            <li class="breadcrumb-item active" aria-current="page">Update</li>
        </ol>
    </nav>
</div>

<div class="card card-dark p-0">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <div>
                    <h4 class="m-0 text-light"><i class="fas fa-cubes mr-1"></i><?= $this->title ?></h4>
                </div><!-- /.col -->
            </div>
        </div>
    </div>

    <?= $this->render('_form', [
        'modelItem' => $modelItem,
        'modelInventory' => $modelInventory,
        'initialPreview'=>$initialPreview,
        'initialPreviewConfig'=>$initialPreviewConfig,
    ]) ?>

</div>
