<?php

use common\models\Category;
use common\models\Location;
use common\models\SubCategory;
use kartik\date\DatePicker;
use kartik\bs4dropdown\Dropdown;
use kartik\widgets\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $modelItem common\models\Item */
/* @var $modelInventory [] common\models\Inventory */
/* @var $form yii\widgets\ActiveForm */
/* @var $initialPreview */
/* @var $initialPreviewConfig */

$locs = Location::find()->all();
?>

<div class="card-body">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div ng-app="dynamicApp" ng-controller="dynamicController" ng-init='rows = <?= $modelInventory ?>;'>
        <?= $form->errorSummary($modelItem) ?>
        <?= $form->field($modelItem, 'ref')->hiddenInput(['maxlength' => 50])->label(false); ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Item Name</label>
            <div class="col-sm-10">
                <?= $form->field($modelItem, 'name')->textInput(['maxlength' => true, 'placeholder' => ''])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Item Code</label>
            <div class="col-sm-10">
                <?= $form->field($modelItem, 'item_code')->textInput(['maxlength' => true, 'placeholder' => ''])->label(false) ?>
            </div>
        </div>

        <hr/>

        <div class="card card-dark card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    Inventory
                </h3>

            </div>
            <div class="card-body">
                <div>
                    <div class="form-group pt-2">
                        <div class="row">
                            <div class="col-md-5 col-12"><label>Location</label></div>
                            <div class="col-md-2 col-3"><label>Quantity</label></div>
                            <div class="col-md-2 col-3"><label>Ideal Qty</label></div>
                            <div class="col-md-2 col-3"><label>Warn Qty</label></div>
                            <div class="col-md-1 col-3"><label>Remove</label></div>
                        </div>
                    </div>

                    <fieldset ng-repeat="row in rows">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5 col-12">
                                    <select name="name[]" class="form-control select2"
                                            style="width: 100%;">
                                        <option selected="selected">{{row.name}}</option>
                                        <?php foreach ($locs as $l): ?>
                                            <option><?= $l->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="col-md-2 col-3 pt-1 pt-md-0">
                                    <input type="text" name="qty[]" class="form-control" value="{{row.qty}}"/>
                                </div>

                                <div class="col-md-2 col-3 pt-1 pt-md-0">
                                    <input type="text" name="idel_qty[]" class="form-control"
                                           value="{{row.ideal_qty}}"/>
                                </div>

                                <div class="col-md-2 col-3 pt-1 pt-md-0">
                                    <input type="text" name="warn_qty[]" class="form-control" value="{{row.warn_qty}}"/>
                                </div>

                                <div class="col-md-1 col-3 pt-1 pt-md-0">
                                    <div class="pt-1">
                                        <button type="button" name="remove"
                                                class="btn btn-danger btn-sm" ng-click="removeRow($index)"><span
                                                    class="fa fa-minus"></span></button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group">
                        <button type="button" name="add_more" class="btn btn-success" ng-click="addRow()"><span
                                    class="fa fa-plus"></span> Add
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <hr/>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Manufactory Part</label>
            <div class="col-sm-10">
                <?= $form->field($modelItem, 'mfr_part')->textInput(['maxlength' => true, 'placeholder' => ''])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Company Part</label>
            <div class="col-sm-10">
                <?= $form->field($modelItem, 'company_part')->textInput(['maxlength' => true, 'placeholder' => ''])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <?= $form->field($modelItem, 'description')->textInput(['maxlength' => true, 'placeholder' => ''])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Cost</label>
            <div class="col-sm-10">
                <?= $form->field($modelItem, 'cost')->textInput(['maxlength' => true, 'placeholder' => ''])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Price</label>
            <div class="col-sm-10">
                <?= $form->field($modelItem, 'price')->textInput(['maxlength' => true, 'placeholder' => ''])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Currency</label>
            <div class="col-sm-10">
                <?=
                $form->field($modelItem, 'currency')->widget(Select2::classname(), [
                    'name' => 'currency',
                    'hideSearch' => false,
                    'data' => ArrayHelper::map(\common\models\Currency::find()->all(), 'code', 'code'),
                    'options' => ['placeholder' => 'Select Currency...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false);

                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Category</label>
            <div class="col-sm-10">
                <?= $form->field($modelItem, 'category_id')
                    ->dropDownList(
                        ArrayHelper::map(Category::find()->all(), 'id', 'name'),
                        [
                            'id' => 'cat-id',
                            'prompt' => 'Select Category',
                        ]
                    )
                    ->label(false)
                ?>

            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Sub Category</label>
            <div class="col-sm-10">
                <?=
                $form->field($modelItem, 'sub_category_id')->widget(DepDrop::classname(), [
                    'options' => ['id' => 'subcat-id'],
                    'data' => ArrayHelper::map(SubCategory::find()->where(['category_id' => $modelItem->category_id])->all(), 'id', 'name'),
                    'pluginOptions' => [
                        'depends' => ['cat-id'],
                        'placeholder' => 'Select...',
                        'url' => Url::to(['/category/subcat'])
                    ]
                ])->label(false);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Measure</label>
            <div class="col-sm-10">
                <?= $form->field($modelItem, 'measure')->textInput(['maxlength' => true, 'placeholder' => ''])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Receive Item Date</label>
            <div class="col-sm-10">
                <?=
                $form->field($modelItem, 'receive_date')->widget(DatePicker::classname(), [
                    'name' => 'receive_date',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'orientation' => 'top right',
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                    ]
                ])->label(false);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Supplier</label>
            <div class="col-sm-10">
                <?=
                $form->field($modelItem, 'supplier_id')->widget(Select2::classname(), [
                    'name' => 'supplier_id',
                    'hideSearch' => false,
                    'data' => ArrayHelper::map(\common\models\Supplier::find()->all(), 'id', 'supplier_name'),
                    'options' => ['placeholder' => 'Select Supplier...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Remark</label>
            <div class="col-sm-10">
                <?= $form->field($modelItem, 'remark')->textarea(['rows' => 3, 'placeholder' => ''])->label(false) ?>
            </div>
        </div>

        <div class="row pb-2">
            <div class="col-12">
                <div class="form-group field-upload_files">
                    <label class="control-label" for="upload_files[]"> Images </label>
                    <div>
                        <?=
                        FileInput::widget([
                            'name' => 'upload_ajax[]',
                            'options' => ['class' => 'align-center', 'multiple' => true, 'accept' => 'image'],
                            'pluginOptions' => [
                                'overwriteInitial' => false,
                                'initialPreviewShowDelete' => true,
                                'initialPreview' => $initialPreview,
                                'initialPreviewConfig' => $initialPreviewConfig,
                                'uploadUrl' => Url::toRoute(['item/upload-ajax']),
                                'browseClass' => 'btn btn-dark',
                                'uploadClass' => 'btn btn-info',
                                'removeClass' => 'btn btn-danger',
                                'uploadExtraData' => [
                                    'ref' => $modelItem->ref,
                                ],
                                'maxFileCount' => 20
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>

        </div>

        <div class="form-group pb-3">
            <div class="row">
                <div class="col-12">
                    <div class="float-right">
                        <?= Html::submitButton($modelItem->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-info']) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

<script src="<?= Url::to('@web/js/angular/angular.min.js') ?>"></script>
<script type="text/javascript" src="<?= Url::to('@web/js/dynamicController.js') ?>"></script>
