<?php

use common\models\SubCategory;
use common\models\Supplier;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Item */
/* @var $modelInventory [] common\models\Inventory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="mt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a
                        href="<?php echo Url::toRoute(['item/index']) ?>">Item</a></li>
            <li class="breadcrumb-item active" aria-current="page">View</li>
        </ol>
    </nav>
</div>

<div class="item-view">
    <?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user'),]) ?>

    <div class="card card-dark">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <div>
                        <h4 class="m-0 text-light"><i class="fas fa-cubes mr-1"></i><?= $this->title ?></h4>
                    </div><!-- /.col -->
                </div>
                <div class="col-4">
                    <div class="float-right <?php echo (Yii::$app->user->can('manage-item')) ? 'visible' : 'invisible' ?>">
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger mt-1 mt-md-0',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-body">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    'item_code',
                    'mfr_part',
                    'company_part',
                    'description',
                    [
                        'attribute' => 'cost',
                        'value' => function ($model) {
                            $c = ($model->currency == null) ? '' : $model->currency;
                            return ($model->cost == null) ? ('') : ($model->cost . ' ' . $c);
                        },
                    ],
                    [
                        'attribute' => 'price',
                        'value' => function ($model) {
                            $c = ($model->currency == null) ? '' : $model->currency;
                            return ($model->price == null) ? '' : $model->price . ' ' . $c;
                        },
                    ],
                    [
                        'attribute' => 'category',
                        'value' => function ($model) {
                            $cat = \common\models\Category::find()->where(['id' => $model->category_id])->one();
                            return ($cat->name == null) ? '' : $cat->name;
                        },
                    ],
                    [
                        'attribute' => 'Sub Category',
                        'value' => function ($model) {
                            $sub = ArrayHelper::map(SubCategory::find()->where(['id' => $model->sub_category_id])->all(), 'id', 'name');

                            return ($sub) ? implode(", ", $sub) : '';
                        },
                    ],
                    [
                        'attribute' => 'Amount',
                        'headerOptions' => ['style' => 'width:100px;'],
                        'value' => function ($model) {
                            $m = ($model->measure == null) ? '' : $model->measure;
                            return ($model->qty == null) ? '' : $model->qty . ' ' . $m;
                        },
                    ],
                    [
                        'attribute' => 'Supplier',
                        'value' => function ($model) {
                            $sup = Supplier::find()->where(['id' => $model->supplier_id])->one();
                            $name = '';
                            if ($sup) {
                                $name = ($sup->supplier_name == null) ? '' : $sup->supplier_name;
                            }
                            return $name;
                        },
                    ],
                    [
                        'attribute' => 'receive_date',
                        'value' => function ($model) {
                            return ($model->receive_date == null) ? '' : $model->receive_date;
                        },
                    ],
                    [
                        'attribute' => 'Remark',
                        'value' => function ($model) {
                            return ($model->remark == null) ? '' : $model->remark;
                        },
                    ],

                ],
            ]) ?>

            <div class="card card-dark card-outline mt-3">
                <div class="card-header">
                    <h3 class="card-title">
                        Inventory
                    </h3>

                </div>
                <div class="card-body">
                    <div>
                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Location</th>
                                        <th>Quantity</th>
                                        <th>Ideal Qty</th>
                                        <th>Warn Qty</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php foreach ($modelInventory as $inv): ?>
                                        <tr>
                                            <td><?= $inv->location->name; ?></td>
                                            <td><?= $inv->qty; ?></td>
                                            <td><?= $inv->ideal_qty; ?></td>
                                            <td><?= $inv->warn_qty; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                    </div>
                </div>
            </div>

            <div class="mt-2">
                <?= dosamigos\gallery\Gallery::widget(['items' => $model->getThumbnails($model->ref, $model->name, 100)]); ?>
            </div>
        </div>
    </div>

</div>
