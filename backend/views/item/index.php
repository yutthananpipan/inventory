<?php

use common\models\SubCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Items';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="item-index mt-3">

    <div class="card card-dark">

        <!--header-->
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <div>
                        <h4 class="m-0 text-light"><i class="fas fa-cubes mr-1"></i><?= $this->title ?></h4>
                    </div><!-- /.col -->
                </div>
                <div class="col-4">
                    <div class="float-right <?php echo (Yii::$app->user->can('manage-item')) ? 'visible' : 'invisible' ?>">
                        <?= Html::a('Retrieves', ['retrieves'], ['class' => 'btn btn-success']) ?>
                        <?= Html::a('<span>Create</span>', ['create'], ['class' => 'btn btn-info mt-1 mt-md-0']) ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- end header-->

        <!--        body-->
        <div class="card-body p-0">

            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'options' => ['class' => 'table-hover table-responsive project'],
                'headerRowOptions' => ['class' => 'text-center'],
                'layout' => "{items}\n{pager}",
                'pager' => [
                    'options' => [
                        'class' => 'pagination  justify-content-center',
                        'style' => ['margin-left' => '15px'],
                    ],

                    'linkContainerOptions' => ['class' => 'page-item'],

                    // Customzing CSS class for pager link
                    'linkOptions' => ['class' => 'page-link'],
                    'activePageCssClass' => 'active',

                    'prevPageCssClass' => 'mypre',
                    'nextPageCssClass' => 'mynext',
                    'firstPageCssClass' => 'myfirst',
                    'lastPageCssClass' => 'mylast',
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'name',
                        'value' => 'name',
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'mfr_part',
                        'value' => function ($model) {
                            return ($model->mfr_part == null) ? '' : $model->mfr_part;
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'company_part',
                        'value' => function ($model) {
                            return ($model->company_part == null) ? '' : $model->company_part;
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'description',
                        'value' => function ($model) {
                            return ($model->description == null) ? '' : $model->description;
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'header' => 'Category',
                        'value' => function ($model) {
                            $cat = \common\models\Category::find()->where(['id' => $model->category_id])->one();
                            return ($cat->name == null) ? '' : $cat->name;
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'header' => 'Sub Category',
                        'value' => function ($model) {
                            $sub = ArrayHelper::map(SubCategory::find()->where(['id' => $model->sub_category_id])->all(), 'id', 'name');

                            return ($sub) ? implode(", ", $sub) : '';
                        },
                        'enableSorting' => false,
                    ],
                    /*[
                        'attribute' => 'cost',
                        'value' => function ($model) {
                            $c = ($model->currency == null) ? '' : $model->currency;
                            return ($model->cost == null) ? ('') : ($model->cost . ' ' . $c);
                        },
                        'enableSorting' => false,
                        'filterOptions' => ['class' => 'invisible']
                    ],
                    [
                        'attribute' => 'price',
                        'value' => function ($model) {
                            $c = ($model->currency == null) ? '' : $model->currency;
                            return ($model->price == null) ? '' : $model->price . ' ' . $c;
                        },
                        'enableSorting' => false,
                        'filterOptions' => ['class' => 'invisible']
                    ],*/
                    [
                        'header' => 'Quantity',
                        'value' => function ($model) {
                            $m = ($model->measure == null) ? '' : $model->measure;

                            $inv = \common\models\Inventory::find()
                                ->select(['SUM(qty) AS sum_qty', 'item_id'])
                                ->where(['item_id' => $model->id])
                                ->groupBy(['item_id'])
                                ->one();

                            return ($inv == null) ? '0' : (($inv->sum_qty == null) ? '0' : $inv->sum_qty . ' ' . $m);

                        },
                        'enableSorting' => false,
                    ],
                    [
                        'header' => 'image',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::img($model->getThumbnail($model->ref, $model->name)['src'], ['options' => ['class' => 'img-fluid']]);
                        }
                    ],
                    [
                        'header' => 'Retrieve',
                        'format' => 'raw',
                        'visible' => (Yii::$app->user->can('manage-item')) ? true : false,
                        'headerOptions' => ['style' => 'width:10px;'],
                        'value' => function ($model) {
                            return Html::a('<i class="fas fa-arrow-alt-circle-right"></i>', Url::toRoute(['retrieve', 'id' => $model->id]), ['class' => 'btn btn-success btn-sm']);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Action',
                        'visible' => (Yii::$app->user->can('manage-item')) ? true : false,
                        'template' => '<div class="btn-group btn-group-sm project-actions text-right" role="group">{view}{update} {delete} </div>',
                        'headerOptions' => ['style' => 'width:235px;'],
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye mr-1"></i>View', $url, ['class' => 'btn btn-warning btn-sm']);
                            },
                            'update' => function ($url, $model, $key) {
                                return Html::a('<i class="fas fa-pen mr-1"></i>Edit', $url, ['class' => 'btn btn-info btn-sm']);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="fas fa-trash mr-1"></i>Delete', $url, [
                                    'class' => 'btn btn-danger btn-sm',
                                    'data-confirm' => Yii::t('user', 'Are you sure you want to delete this data?'),
                                    'data-method' => 'POST',
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>
        <!--        end body-->
    </div>

</div>
