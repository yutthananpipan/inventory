<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $modelItem common\models\Item */
/* @var $modelRetrieve \common\models\Retrieve ?> */
/* @var $modelInventory common\models\Inventory */
/* @var $locations [] common\models\Location */


$this->title = 'Retrieve Item: ' . $modelItem->name;
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $modelItem->name, 'url' => ['view', 'id' => $modelItem->id]];
$this->params['breadcrumbs'][] = 'Retrieve';

?>
<div class="mt-3">

    <?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user'),]) ?>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a
                        href="<?php echo Url::toRoute(['item/index']) ?>">Item</a></li>
            <li class="breadcrumb-item active" aria-current="page">Retrieve</li>
        </ol>
    </nav>
</div>

<div class="card card-dark p-0">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <div>
                    <h4 class="m-0 text-light"><i class="fas fa-cubes mr-1"></i>Retrieve</h4>
                </div><!-- /.col -->
            </div>
        </div>
    </div>

    <div class="card-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div ng-app="" ng-init='inventory = <?= $modelInventory ?>;'>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Item Name</label>
                <div class="col-sm-10">
                    <?= $form->field($modelItem, 'name')->textInput(['maxlength' => true, 'placeholder' => '', 'readonly' => true])->label(false) ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Item Code</label>
                <div class="col-sm-10">
                    <?= $form->field($modelItem, 'item_code')->textInput(['maxlength' => true, 'placeholder' => '', 'readonly' => true])->label(false) ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Location</label>
                <div class="col-sm-10">
                    <?=
                    $form->field($modelRetrieve, 'location_id')->widget(Select2::classname(), [
                        'name' => 'location_id',
                        'hideSearch' => false,
                        'data' => $locations,
                        'options' => ['placeholder' => 'Select Location...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Retrieve Quantity</label>
                <div class="col-sm-10">
                    <?= $form->field($modelRetrieve, 'qty')->textInput(['maxlength' => true, 'placeholder' => '', 'ng-model' => 'qty', 'class' => 'form-control'])->label(false) ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Project</label>
                <div class="col-sm-10">
                    <?=
                    $form->field($modelRetrieve, 'project_id')->widget(Select2::classname(), [
                        'name' => 'project_id',
                        'hideSearch' => false,
                        'data' => ArrayHelper::map(\common\models\Project::find()->all(), 'pro_id', 'pro_name'),
                        'options' => ['placeholder' => 'Select Project...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                    ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Retrieve Date</label>
                <div class="col-sm-10">
                    <?=
                    $form->field($modelRetrieve, 'retrieve_date')->widget(DatePicker::classname(), [
                        'name' => 'receive_date',
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'orientation' => 'top right',
                            'format' => 'yyyy-mm-dd',
                            'autoclose' => true,
                        ]
                    ])->label(false);
                    ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Retrieve Name</label>
                <div class="col-sm-10">
                    <?= $form->field($modelRetrieve, 'retrieve_name')->textInput(['maxlength' => true, 'placeholder' => ''])->label(false) ?>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Current Quantity</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputError" placeholder=""
                           value="{{inventory.sum_qty - qty}}" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Ideal Quantity</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputError" value="{{inventory.sum_ideal_qty}}"
                           placeholder="" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Warning Quantity</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputError" value="{{inventory.sum_warn_qty}}"
                           placeholder="" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                    <?= $form->field($modelRetrieve, 'description')->textarea(['rows' => 3, 'placeholder' => ''])->label(false) ?>
                </div>
            </div>

            <div class="form-group pb-3">
                <div class="row">
                    <div class="col-12">
                        <div class="float-right">
                            <?= Html::submitButton('Retrieve', ['class' => 'btn btn-info']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>

<script src="<?= Url::to('@web/js/angular/angular.min.js') ?>"></script>