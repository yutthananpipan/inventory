<?php

use common\models\SubCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="mt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a
                        href="<?php echo Url::toRoute(['category/index']) ?>">Item</a></li>
            <li class="breadcrumb-item active" aria-current="page">View</li>
        </ol>
    </nav>
</div>

<div class="category-view">
    <?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user'),]) ?>

    <div class="card card-dark">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <div>
                        <h4 class="m-0 text-light"><i class="fas fa-object-group mr-1"></i><?= $this->title ?></h4>
                    </div><!-- /.col -->
                </div>
                <div class="col-4">
                    <div class="float-right <?php echo (Yii::$app->user->can('manage-item')) ? 'visible' : 'invisible' ?>">
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger mt-1 mt-md-0',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-body">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    [
                        'attribute' => 'Sub Category',
                        'value' => function ($model) {
                            $sub = ArrayHelper::map(SubCategory::find()->where(['category_id' => $model->id])->all(), 'id', 'name');

                            return ($sub) ? implode(", ", $sub) : '';
                        },
                    ]
                ],
            ]) ?>

        </div>
    </div>

</div>
