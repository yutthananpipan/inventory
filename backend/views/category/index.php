<?php

use common\models\SubCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="category-index mt-3">

    <div class="card card-dark">
        <!--header-->
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <div>
                        <h4 class="m-0 text-light"><i class="fas fa-object-group mr-1"></i><?= $this->title ?></h4>
                    </div><!-- /.col -->
                </div>
                <div class="col-4">
                    <div class="float-right <?php echo (Yii::$app->user->can('manage-category')) ? 'visible' : 'invisible' ?>">
                        <?= Html::a('<span>Create</span>', ['create'], ['class' => 'btn btn-info']) ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- end header-->

        <div class="card-body p-0">

            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'options' => ['class' => 'table-hover table-responsive project'],
                'headerRowOptions' => ['class' => 'text-center'],
                'layout' => "{items}\n{pager}",
                'pager' => [
                    'options' => [
                        'class' => 'pagination  justify-content-center',
                        'style' => ['margin-left' => '15px'],
                    ],

                    'linkContainerOptions' => ['class' => 'page-item'],

                    // Customzing CSS class for pager link
                    'linkOptions' => ['class' => 'page-link'],
                    'activePageCssClass' => 'active',

                    'prevPageCssClass' => 'mypre',
                    'nextPageCssClass' => 'mynext',
                    'firstPageCssClass' => 'myfirst',
                    'lastPageCssClass' => 'mylast',
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'header' => 'category',
                        'value' => 'name',
                        'enableSorting' => false,
                    ],
                    [
                        'header' => 'Sub Category',
                        'value' => function ($model) {

                            $sub = ArrayHelper::map(SubCategory::find()->where(['category_id' => $model->id])->all(), 'id', 'name');

                            return ($sub) ? implode(", ", $sub) : '';
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Action',
                        'visible' => (Yii::$app->user->can('manage-category')) ? true : false,
                        'template' => '<div class="btn-group btn-group-sm project-actions text-right" role="group">{update} {delete} </div>',
                        'options' => ['style' => 'width:170px;'],
                        'buttons' => [
                            'update' => function ($url, $model, $key) {
                                return Html::a('<i class="fas fa-pen mr-1"></i>Edit', $url, ['class' => 'btn btn-info btn-sm']);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="fas fa-trash mr-1"></i>Delete', $url, [
                                    'class' => 'btn btn-danger btn-sm',
                                    'data-confirm' => Yii::t('user', 'Are you sure you want to delete this data?'),
                                    'data-method' => 'POST',
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
