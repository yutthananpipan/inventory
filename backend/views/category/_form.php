<?php

use common\models\SubCategory;
use phpDocumentor\Reflection\Types\Object_;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $modelCategory common\models\Category */
/* @var $modelSubCategorys [] common\models\SubCategory */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="card-body">

    <?php $form = ActiveForm::begin(); ?>

    <div ng-app="dynamicApp" ng-controller="dynamicController" ng-init='rows = <?= $modelSubCategorys ?>;'>

        <?= $form->field($modelCategory, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Category Name']) ?>

        <div class="card card-dark card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    Sub Category
                </h3>
            </div>
            <div class="card-body">
                <div>
                    <fieldset ng-repeat="row in rows">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-10 col-md-11">
                                    <input type="text" name="sub_category[]" class="form-control" value="{{row.name}}"/>
                                </div>
                                <div class="col-2 col-md-1">
                                    <div class="pt-1">
                                        <button type="button" name="remove"
                                                class="btn btn-danger btn-sm" ng-click="removeRow($index)"><span
                                                    class="fa fa-minus"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group">
                        <button type="button" name="add_more" class="btn btn-success" ng-click="addRow()"><span
                                    class="fa fa-plus"></span> Add
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group pb-3">
            <div class="row">
                <div class="col-12">
                    <div class="float-right">
                        <?= Html::submitButton($modelCategory->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-info']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script src="<?= Url::to('@web/js/angular/angular.min.js') ?>"></script>
<script type="text/javascript" src="<?= Url::to('@web/js/dynamicController.js') ?>"></script>