<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $modelCategory common\models\Category */
/* @var $modelSubCategorys[] common\models\SubCategory */

$this->title = 'Update Category: ' . $modelCategory->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $modelCategory->name, 'url' => ['view', 'id' => $modelCategory->id]];
$this->params['breadcrumbs'][] = 'Update';
?>


<div class="mt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a
                        href="<?php echo Url::toRoute(['category/index']) ?>">Item</a></li>
            <li class="breadcrumb-item active" aria-current="page">Update</li>
        </ol>
    </nav>
</div>

<div class="card card-dark p-0 mt-3">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <div>
                    <h4 class="m-0 text-light"><i class="fas fa-object-group mr-1"></i><?= $this->title ?></h4>
                </div><!-- /.col -->
            </div>
        </div>
    </div>

    <?= $this->render('_form', [
        'modelCategory' => $modelCategory,
        'modelSubCategorys' => $modelSubCategorys,
    ]) ?>

</div>
