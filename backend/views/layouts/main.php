<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="<?php echo Url::to('@web/img/inventory-icon.ico'); ?>"/>
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <?php $this->head() ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<?php $this->beginBody() ?>
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light <?php if (Yii::$app->user->isGuest) echo 'd-none' ?>">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo Url::to('@web/item/create') ?>" class="nav-link">New Item</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo Url::to('@web/category/create') ?>" class="nav-link">New Category</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo Url::to('@web/item/retrieves') ?>" class="nav-link">Retrieves</a>
            </li>
        </ul>

        <!-- SEARCH FORM -->
        <?php $form = ActiveForm::begin(['action' => ['/item/index']]); ?>
        <div class="form-inline ml-3">
            <div class="input-group input-group-sm">
                <input name="itemsearch" class="form-control form-control-navbar" type="search"
                       placeholder="Search Item"
                       aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-info bg-light elevation-4 <?php if (Yii::$app->user->isGuest) echo 'd-none' ?>">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            <img src="<?php echo Url::to('@web/img/inventory-icon.png'); ?>" alt="Inventory Logo"
                 class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-bold" style="color: #555">Inventory</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="<?php echo Url::to('@web/AdminLTE-master/dist/img/avatar6.jpg'); ?>"
                         class="img-circle elevation-2"
                         alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">
                        <?php
                        if (!Yii::$app->user->isGuest) {
                            echo Yii::$app->user->identity->username;
                        }
                        ?>
                    </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->

                    <li class="nav-item">
                        <a id="dashboard" href="<?php echo Url::to('@web/site/index') ?>" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a id="item" href="<?php echo Url::to('@web/item') ?>" class="nav-link">
                            <i class="nav-icon fas fa-cubes"></i>
                            <p>Item</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a id="project" href="<?php echo Url::to('@web/project') ?>" class="nav-link">
                            <i class="nav-icon fab fa-product-hunt"></i>
                            <p>Projects</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a id="category" href="<?php echo Url::to('@web/category') ?>" class="nav-link">
                            <i class="nav-icon fas fa-object-group"></i>
                            <p>Categories</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a id="location" href="<?php echo Url::to('@web/location') ?>" class="nav-link">
                            <i class="nav-icon fas fa-map-marked"></i>
                            <p>Locations</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a id="supplier" href="<?php echo Url::to('@web/supplier') ?>" class="nav-link">
                            <i class="nav-icon fas fa-shopping-cart"></i>
                            <p>Suppliers</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a id="retrieve-report" href="<?php echo Url::to('@web/item/retrieve-report') ?>" class="nav-link">
                            <i class="nav-icon fas fa-arrow-alt-circle-right"></i>
                            <p>Retrieve Report</p>
                        </a>
                    </li>

                    <li class="nav-header">user management</li>
                    <li class="nav-item">
                        <a id="profile" href="<?php echo Url::to('@web/user/settings/profile') ?>" class="nav-link">
                            <i class="nav-icon fas fa-user"></i>
                            <p>Profile</p>
                        </a>
                    </li>
                    <li class="nav-item <?php if (!Yii::$app->user->can('user-admin')) echo 'd-none' ?>">
                        <a id="user-admin" href="<?php echo Url::to('@web/user/admin') ?>" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>Users</p>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li class="nav-item">
                        <a id="user-admin" href="<?php echo Url::to('@web/user/security/logout') ?>" class="nav-link">
                            <i class="nav-icon fas fa-sign-out-alt"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                    <li class="text-light">li</li>
                    <li class="text-light">li</li>
                    <li class="text-light">li</li>
                    <li class="text-light">li</li>
                    <li class="text-light">li</li>
                    <li class="text-light">li</li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="<?php echo (Yii::$app->user->isGuest) ? '' : 'content-wrapper' ?>">

        <!-- Main content -->
        <section class="content">
            <div class="<?php echo (Yii::$app->user->isGuest) ? '' : 'container-fluid' ?>">
                <?= $content ?>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer <?php if (Yii::$app->user->isGuest) echo 'd-none' ?>">
        <strong>Copyright &copy; 2020 <a href="http://www.drc.co.th" class="text-info">DRC Solution
                Inspired</a>.</strong>
        All rights reserved.

    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
