<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-body">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'project_no')->textInput(['maxlength' => true, 'placeholder' => '']) ?>

    <?= $form->field($model, 'pro_name')->textInput(['maxlength' => true, 'placeholder' => '']) ?>

    <?= $form->field($model, 'project_desc')->textInput(['maxlength' => true, 'placeholder' => '']) ?>

    <div class="form-group pb-3">
        <div class="row">
            <div class="col-12">
                <div class="float-right">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-info']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
