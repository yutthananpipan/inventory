<?php


/* @var $this yii\web\View */

/**
 * @var string $item
 * @var string $project
 * @var string $category
 * @var string $location
 * @var string[] $category_graph
 * @var string[] $location_graph
 * @var string[] $retrieve_graph
 */

use miloschuman\highcharts\Highcharts;
use yii\helpers\Url;

$this->title = 'Dashboard';
?>

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
            <div class="inner">
                <h3><?= $item ?></h3>

                <p>Items</p>
            </div>
            <div class="icon">
                <i class="fas fa-cubes"></i>
            </div>
            <a href="<?= Url::toRoute(['/item']) ?>" class="small-box-footer">More info
                <i class="fas fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-warning">
            <div class="inner">
                <h3><?= $project ?></h3>

                <p>Projects</p>
            </div>
            <div class="icon">
                <i class="fab fa-product-hunt"></i>
            </div>
            <a href="<?= Url::toRoute(['/project']) ?>" class="small-box-footer">More info <i
                        class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
            <div class="inner">
                <h3><?= $category ?></h3>

                <p>Categories</p>
            </div>
            <div class="icon">
                <i class="fas fa-object-group"></i>
            </div>
            <a href="<?= Url::toRoute(['/category']) ?>" class="small-box-footer">More info <i
                        class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
            <div class="inner">
                <h3><?= $location ?></h3>

                <p>Locations</p>
            </div>
            <div class="icon">
                <i class="fas fa-map-marked"></i>
            </div>
            <a href="<?= Url::toRoute(['/location']) ?>" class="small-box-footer">More info <i
                        class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->

</div>
<!-- /.row -->
<!-- Main row -->
<div class="row">
    <!-- /.col -->
    <div class="col-md-6">
        <!-- Donut chart -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fas fa-chart-pie"></i>
                    Category Chart
                </h3>

            </div>
            <div class="card-body">
                <div style="height: auto;">
                    <?php
                    echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'backgroundColor' => 'transparent',
                                'plotBackgroundColor' => null,
                                'plotBorderWidth' => null,
                                'plotShadow' => false,
                                'type' => 'pie'
                            ],
                            'tooltip' => ['pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>'],
                            'title' => ['text' => 'Categories', 'format' => '',],
                            'plotOptions' => [
                                'pie' => [
                                    'allowPointSelect' => true,
                                    'cursor' => 'pointer',
                                    'showInLegend' => true,
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'format' => '{point.percentage:.1f} %',
                                        'distance' => -50,
                                        'filter' => [
                                            'property' => 'percentage',
                                            'operator' => '>',
                                            'value' => 4
                                        ]
                                    ]
                                ],
                            ],
                            'series' => [
                                ['name' => 'Graph Categories', 'data' => $category_graph
                                ],
                                //['name' => 'ข้อมูลการสำรวจ', 'data' => $graph]
                            ],
                            'credits' => ['enabled' => false],
                        ]
                    ]);
                    ?>
                </div>
            </div>
            <!-- /.card-body-->
        </div>
        <!-- /.card -->
    </div>

    <div class="col-md-6">
        <!-- Line chart -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fas fa-chart-pie"></i>
                    Locations Chart
                </h3>

            </div>
            <div class="card-body">
                <div style="height: auto;">
                    <?php
                    echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'backgroundColor' => 'transparent',
                                'plotBackgroundColor' => null,
                                'plotBorderWidth' => null,
                                'plotShadow' => false,
                                'type' => 'pie'
                            ],
                            'tooltip' => ['pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>'],
                            'title' => ['text' => 'Locations', 'format' => '',],
                            'plotOptions' => [
                                'pie' => [
                                    'allowPointSelect' => true,
                                    'cursor' => 'pointer',
                                    'showInLegend' => true,
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'format' => '{point.percentage:.1f} %',
                                        'distance' => -50,
                                        'filter' => [
                                            'property' => 'percentage',
                                            'operator' => '>',
                                            'value' => 4
                                        ]
                                    ]
                                ],
                            ],
                            'series' => [
                                ['name' => 'Graph Locations', 'data' => $location_graph
                                ],
                                //['name' => 'ข้อมูลการสำรวจ', 'data' => $graph]
                            ],
                            'credits' => ['enabled' => false],
                        ]
                    ]);
                    ?>
                </div>
            </div>
            <!-- /.card-body-->
        </div>
        <!-- /.card -->

    </div>
    <!-- /.col -->

    <div class="col-12">
        <!-- Line chart -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="far fa-chart-bar"></i>
                    Retrieve Graph
                </h3>

            </div>
            <div class="card-body">
                <div style="height: auto;">
                    <?php
                    echo Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'backgroundColor' => 'transparent',
                                'plotBackgroundColor' => null,
                                'plotBorderWidth' => null,
                                'plotShadow' => false,
                            ],
                            'title' => ['text' => ''],
                            'xAxis' => [
                                'categories' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                            ],
                            'yAxis' => [
                                'title' => ['text' => 'Time']
                            ],
                            'series' => $retrieve_graph,
                            'credits' => ['enabled' => false],
                        ]
                    ]);
                    ?>
                </div>
            </div>
            <!-- /.card-body-->
        </div>
        <!-- /.card -->

    </div>
    <!-- /.col -->
</div>
<!-- /.row (main row) -->
