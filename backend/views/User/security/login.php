<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use common\models\LoginForm;
use dektrium\user\widgets\Connect;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */

?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="<?php echo Url::to('@web/img/inventory-icon.ico'); ?>"/>
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet"
          href="<?php echo Url::to('@web/AdminLTE-master/plugins/fontawesome-free/css/all.min.css'); ?>">
    <link rel="stylesheet"
          href="<?php echo Url::to('@web/AdminLTE-master/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'); ?>">
    <link rel="stylesheet"
          href="<?php echo Url::to('@web/AdminLTE-master/plugins/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo Url::to('@web/AdminLTE-master/plugins/jqvmap/jqvmap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo Url::to('@web/AdminLTE-master/dist/css/adminlte.min.css'); ?>">
    <link rel="stylesheet"
          href="<?php echo Url::to('@web/AdminLTE-master/plugins/overlayScrollbars/css/OverlayScrollbars.min.css'); ?>">
    <link rel="stylesheet"
          href="<?php echo Url::to('@web/AdminLTE-master/plugins/daterangepicker/daterangepicker.css'); ?>">
    <link rel="stylesheet" href="<?php echo Url::to('@web/AdminLTE-master/plugins/summernote/summernote-bs4.css'); ?>">
    <link rel="stylesheet" href="<?php echo Url::to('@web/css/inventory.css'); ?>">

</head>
<body class="hold-transition login-page auth auth-bg-1">

<div class="login-box">

    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
            ]) ?>
            <div class="mb-3">
                <?php if ($module->debug): ?>
                    <?= $form->field($model, 'login', [
                        'placeholder' => 'User',
                        'inputOptions' => [
                            'autofocus' => 'autofocus',
                            'class' => 'form-control',
                            'tabindex' => '1']])->dropDownList(LoginForm::loginList());
                    ?>

                <?php else: ?>

                    <?= $form->field($model, 'login',
                        [
                            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1', 'placeholder' => 'User',],

                        ]
                    );
                    ?>

                <?php endif ?>
            </div>
            <div class="mb-3">
                <?php if ($module->debug): ?>
                    <div class="alert alert-warning">
                        <?= Yii::t('user', 'Password is not necessary because the module is in DEBUG mode.'); ?>
                    </div>
                <?php else: ?>
                    <?= $form->field(
                        $model,
                        'password',
                        ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])
                        ->passwordInput(['placeholder' => 'Password',])
                    ?>
                <?php endif ?>
            </div>
            <div class="row">
                <div class="col-12">
                    <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '3']) ?>
                </div>
                <!-- /.col -->
                <div class="col-12">

                    <?= Html::submitButton(
                        Yii::t('user', 'Sign in'),
                        ['class' => 'btn bg-cyan btn-block bg-info', 'tabindex' => '4']
                    ) ?>
                </div>
                <!-- /.col -->
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <!-- /.login-card-body -->
        <?= Connect::widget([
            'baseAuthUrl' => ['/user/security/auth'],
        ]) ?>
    </div>
</div>
<!-- /.login-box -->

<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/chart.js/Chart.min.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/sparklines/sparkline.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/jqvmap/jquery.vmap.min.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/jqvmap/maps/jquery.vmap.usa.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/jquery-knob/jquery.knob.min.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/moment/moment.min.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/daterangepicker/daterangepicker.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/summernote/summernote-bs4.min.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/dist/js/adminlte.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/dist/js/pages/dashboard.js'); ?>"></script>
<script src="<?php echo Url::to('@web/AdminLTE-master/dist/js/demo.js'); ?>"></script>

</body>
</html>
