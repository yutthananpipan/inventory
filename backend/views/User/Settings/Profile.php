<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */

$this->title = Yii::t('user', 'Profile');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user'),]) ?>

<div class="card card-dark mt-2">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <div>
                    <h3 class="m-0 text-light"><?= $this->title ?></h3>
                </div><!-- /.col -->
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-3 ">
            <div class="card-body p-2">
                <?= $this->render('_menu') ?>
            </div>
        </div>

        <div class="col-md-9">
            <div class="card-body">

                <div>
                    <div>
                        <?php $form = ActiveForm::begin([
                            'id' => 'profile-form',
                            'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => false,
                            'validateOnBlur' => false,
                        ]); ?>

                        <div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => false, 'placeholder' => "Name"])->label(false) ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <?= $form->field($model, 'public_email')->textInput(['maxlength' => true, 'placeholder' => "Email"])->label(false) ?>
                                </div>
                            </div>

                        </div>

                        <div class="float-right"><?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn bg-cyan float-right text-uppercase text-right']) ?></div>

                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


