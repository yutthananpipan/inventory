<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\SettingsForm $model
 */

$this->title = Yii::t('user', 'Account');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user'),]) ?>
<div class="card card-dark mt-2">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <div>
                    <h3 class="m-0 text-light"><?= $this->title ?></h3>
                </div><!-- /.col -->
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-3 ">
            <div class="card-body p-2">
                <?= $this->render('_menu') ?>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card-body">


                <div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'account-form',
                        'options' => ['class' => 'form-horizontal'],

                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                    ]); ?>

                    <div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'disabled' => false, 'placeholder' => "Email"])->label(false) ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Username</label>
                            <div class="col-sm-10">
                                <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'disabled' => true, 'placeholder' => "Email"])->label(false) ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">New Password</label>
                            <div class="col-sm-10">
                                <?= $form->field($model, 'new_password')->passwordInput(['maxlength' => true, 'disabled' => false, 'placeholder' => "Password"])->label(false) ?>
                            </div>
                        </div>

                        <hr/>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Current Password</label>
                            <div class="col-sm-10">
                                <?= $form->field($model, 'current_password')->passwordInput(['maxlength' => true, 'disabled' => false, 'placeholder' => "Password"])->label(false) ?>
                            </div>
                        </div>

                    </div>

                    <div class="float-right"><?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn bg-cyan float-right text-uppercase text-right']) ?></div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>

            <?php if ($model->module->enableAccountDelete): ?>
            <div class="filters-content">
                <p>
                    <?= Yii::t('user', 'Once you delete your account, there is no going back') ?>.
                    <?= Yii::t('user', 'It will be deleted forever') ?>.
                    <?= Yii::t('user', 'Please be certain') ?>.
                </p>
                <?= Html::a(Yii::t('user', 'Delete account'), ['delete'], [
                    'class' => 'btn btn-danger',
                    'data-method' => 'post',
                    'data-confirm' => Yii::t('user', 'Are you sure? There is no going back'),
                ]) ?>
            </div>
        </div>
        <?php endif ?>
    </div>
</div>


