<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $user
 * @var common\models\Profile $profile
 */
?>

<?php $this->beginContent('@app/views/User/Admin/update.php', ['user' => $user]) ?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'options' => ['enctype' => 'multipart/form-data'],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-10',
        ],
    ],
]); ?>

<?= $form->field($profile, 'name')->textInput(['maxlength' => true, 'disabled' => false]) ?>

<?= $form->field($profile, 'public_email')->textInput(['maxlength' => true]) ?>

<div style="float: right;" ><?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn bg-cyan float-right text-uppercase text-right']) ?></div>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>
