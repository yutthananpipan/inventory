<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap4\Nav;

?>

<div class="row mb-2 pb-2" style="margin-left: 1px;">
    <?php if (Yii::$app->user->can('department')) echo '<div class="text-center mb-1 mr-2"><a class="menu-item bg-secondary text-uppercase" style="width: 100%" href="?r=department">โครงสร้างหน่วยงาน</a></div>'; ?>
    <?php if (Yii::$app->user->can('user')) echo '<div class="text-center mb-1 mr-2"><a class="menu-item bg-secondary text-uppercase" style="width: 100%" href="?r=user/admin">จัดการสมาชิก</a></div>'; ?>
    <?php if (Yii::$app->user->can('setmediasize')) echo '<div class="text-center mb-1 mr-2"><a class="menu-item bg-secondary text-uppercase" style="width: 100%" href="?r=media/mediatype/update">ตั้งขนาดไฟล์</a></div>'; ?>
    <?php if (Yii::$app->user->can('slide')) echo '<div class="text-center mb-1 mr-2"><a class="menu-item bg-secondary text-uppercase" style="width: 100%" href="?r=slide/view-image">สไลด์</a></div>'; ?>
    <?php if (Yii::$app->user->can('log')) echo '<div class="text-center mb-1 mr-2"><a class="menu-item bg-secondary text-uppercase" style="width: 100%" href="?r=log">ประวัติการใช้งาน</a></div>'; ?>
    <?php if (Yii::$app->user->can('license')) echo '<div class="text-center mb-1 mr-2"><a class="menu-item bg-secondary text-uppercase" style="width: 100%" href="?r=site/license">การอัพเดท</a></div>'; ?>
</div>