<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Nav;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $user
 */

$this->title = Yii::t('user', 'Create a user account');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="mt-3">
    <?= $this->render('_menu') ?>
</div>

<?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user'),]) ?>

<div class="card card-dark mt-3">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <div>
                    <h4 class="m-0 text-light"><?= $this->title ?></h4>
                </div><!-- /.col -->
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="card-body">
                <div class="p-2">
                    <?= Nav::widget([
                        'options' => [
                            'class' => 'nav flex-column nav-pills nav-stacked',
                        ],
                        'items' => [
                            ['label' => Yii::t('user', 'Account details'), 'url' => ['/user/admin/create']],
                            ['label' => Yii::t('user', 'Profile details'), 'options' => [
                                'class' => 'disabled',
                                'onclick' => 'return false;',
                            ]],
                            ['label' => Yii::t('user', 'Information'), 'options' => [
                                'class' => 'disabled',
                                'onclick' => 'return false;',
                            ]],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div>
                <div class="card-body p-4">

                    <div class="alert alert-info">
                        <?= Yii::t('user', 'Credentials will be sent to the user by email') ?>.
                        <?= Yii::t('user', 'A password will be generated automatically if not provided') ?>.
                    </div>
                    <?php $form = ActiveForm::begin([
                        'layout' => 'horizontal',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'fieldConfig' => [
                            'horizontalCssClasses' => [
                                'wrapper' => 'col-sm-10',
                            ],
                        ],
                    ]); ?>

                    <?= $this->render('_user', ['form' => $form, 'user' => $user]) ?>

                    <div style="float: right;">
                        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn bg-cyan float-right text-uppercase float-right']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>





