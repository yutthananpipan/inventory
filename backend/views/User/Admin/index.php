<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;


/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var common\models\UserSearch $searchModel
 */

$this->title = Yii::t('user', 'User Account');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="mt-3">
    <?= $this->render('@app/views/User/Admin/_menu') ?>
</div>

<?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user'),]) ?>

<div class="card card-dark mt-3">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <div>
                    <h4 class="m-0 text-light"><?= $this->title ?></h4>
                </div><!-- /.col -->
            </div>

        </div>
    </div>

    <div class="card-body">

        <?php Pjax::begin() ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'options' => ['class' => 'table-hover table-responsive project'],
            'headerRowOptions' => ['class' => 'text-center'],
            'layout' => "{items}\n{pager}",
            'pager' => [
                'options' => [
                    'class' => 'pagination  justify-content-center',
                    'style' => ['margin-left' => '15px'],
                ],

                'linkContainerOptions' => ['class' => 'page-item'],

                // Customzing CSS class for pager link
                'linkOptions' => ['class' => 'page-link'],
                'activePageCssClass' => 'active',

                'prevPageCssClass' => 'mypre',
                'nextPageCssClass' => 'mynext',
                'firstPageCssClass' => 'myfirst',
                'lastPageCssClass' => 'mylast',
            ],
            'columns' => [
                [
                    'attribute' => 'id',
                    'value' => function ($model) {
                        return $model->id;
                    },
                    'headerOptions' => ['style' => 'width:30px;'],
                    'enableSorting' => false,
                ],
                [
                    'headerOptions' => ['style' => 'width:120px;'],
                    'attribute' => 'username',
                    'value' => 'username',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'roles',
                    'headerOptions' => ['style' => 'width:140px;'],
                    'value' => function ($data) {
                        $roles = \Yii::$app->authManager->getRolesByUser($data->id);
                        if ($roles) {
                            return implode(', ', array_keys($roles));
                        } else {
                            return '';
                        }
                    }

                ],
                [
                    'headerOptions' => ['style' => 'width:140px;'],
                    'attribute' => 'profile.name',
                    'value' => function ($model) {
                        if ($model->profile->name != null) {
                            return $model->profile->name;
                        } else {
                            return '';
                        }
                    },
                ],

                [
                    'attribute' => 'email',
                    'value' => 'email',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'last_login_at',
                    'value' => function ($model) {
                        if (!$model->last_login_at || $model->last_login_at == 0) {
                            return Yii::t('user', 'Never');
                        } else if (extension_loaded('intl')) {
                            return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_login_at]);
                        } else {
                            return date('Y-m-d G:i:s', $model->last_login_at);
                        }
                    },
                    'enableSorting' => false,
                ],
                [
                    'header' => Yii::t('user', 'Confirmation'),
                    'value' => function ($model) {
                        if ($model->isConfirmed) {
                            return '<div class="text-center">
                                <span class="text-success">' . Yii::t('user', 'Confirmed') . '</span>
                            </div>';
                        } else {
                            return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-success btn-block',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                            ]);
                        }
                    },
                    'format' => 'raw',
                    'visible' => Yii::$app->getModule('user')->enableConfirmation,
                ],
                [
                    'header' => Yii::t('user', 'Block status'),
                    'value' => function ($model) {
                        if ($model->isBlocked) {
                            return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                                'class' => 'btn btn-sm btn-success btn-block',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                            ]);
                        } else {
                            return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                                'class' => 'btn btn-sm btn-danger btn-block',
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                            ]);
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '<div class="btn-group btn-group-sm project-actions text-right" role="group">{update} {delete} </div>',
                    'options' => ['style' => 'width:auto;'],
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a('<i class="fas fa-pen mr-1"></i>Edit', $url, ['class' => 'btn btn-info btn-sm']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="fas fa-trash mr-1"></i>Delete', $url, [
                                'class' => 'btn btn-danger btn-sm',
                                'data-confirm' => Yii::t('user', 'Are you sure you want to delete this user?'),
                                'data-method' => 'POST',
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>

        <?php Pjax::end() ?>
    </div>
</div>



