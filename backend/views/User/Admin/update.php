<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap4\Nav;

/**
 * @var \yii\web\View $this
 * @var common\models\User $user
 * @var string $content
 */

$this->title = Yii::t('user', 'Update user account');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="mt-3">
    <?= $this->render('@app/views/User/Admin/_menu') ?>
</div>

<div class="card card-dark mt-3">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <div>
                    <h4 class="m-0 text-light"><?= $this->title ?></h4>
                </div><!-- /.col -->
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="card-body">
                <div class="p-2">
                    <?= Nav::widget([
                        'options' => [
                            'class' => 'nav flex-column nav-pills nav-stacked',
                        ],
                        'items' => [
                            [
                                'label' => Yii::t('user', 'Account'),
                                'url' => ['/user/admin/update', 'id' => $user->id]
                            ],
                            [
                                'label' => Yii::t('user', 'Profile'),
                                'url' => ['/user/admin/update-profile', 'id' => $user->id]
                            ],
                            ['label' => Yii::t('user', 'Status'), 'url' => ['/user/admin/info', 'id' => $user->id]],
                            [
                                'label' => Yii::t('user', 'Manage Account'),
                                'url' => ['/user/admin/assignments', 'id' => $user->id],
                                'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
                            ],
                            '<hr>',
                            [
                                'label' => Yii::t('user', 'Confirm'),
                                'url' => ['/user/admin/confirm', 'id' => $user->id],
                                'visible' => !$user->isConfirmed,
                                'linkOptions' => [
                                    'class' => 'text-success',
                                    'data-method' => 'post',
                                    'data-confirm' => Yii::t('user', 'Confirm this user?'),
                                ],
                            ],
                            [
                                'label' => Yii::t('user', 'Block'),
                                'url' => ['/user/admin/block', 'id' => $user->id],
                                'visible' => !$user->isBlocked,
                                'linkOptions' => [
                                    'class' => 'text-danger',
                                    'data-method' => 'post',
                                    'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                                ],
                            ],
                            [
                                'label' => Yii::t('user', 'Unblock'),
                                'url' => ['/user/admin/block', 'id' => $user->id],
                                'visible' => $user->isBlocked,
                                'linkOptions' => [
                                    'class' => 'text-success',
                                    'data-method' => 'post',
                                    'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                                ],
                            ],
                            [
                                'label' => Yii::t('user', 'Delete Account'),
                                'url' => ['/user/admin/delete', 'id' => $user->id],
                                'linkOptions' => [
                                    'class' => 'text-danger',
                                    'data-method' => 'post',
                                    'data-confirm' => Yii::t('user', 'Are you sure you want to delete this user?'),
                                ],
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div>
                <div class="card-body p-4">
                    <?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user')]) ?>
                    <div class="container">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


