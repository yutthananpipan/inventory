<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Supplier */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-body">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'supplier_name')->textInput(['maxlength' => true, 'placeholder' => '']) ?>

    <?= $form->field($model, 'contact_persion')->textInput(['maxlength' => true, 'placeholder' => '']) ?>

    <?= $form->field($model, 'contact_firstname')->textInput(['maxlength' => true,'placeholder' => '']) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6, 'placeholder' => '']) ?>

    <?= $form->field($model, 'phone_primary')->textInput(['maxlength' => true, 'placeholder' => '']) ?>

    <?= $form->field($model, 'phone_alternative')->textInput(['maxlength' => true, 'placeholder' => '']) ?>

    <?= $form->field($model, 'fax')->textInput(['maxlength' => true, 'placeholder' => '']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => '']) ?>

    <div class="form-group pb-3">
        <div class="row">
            <div class="col-12">
                <div class="float-right">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-info']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
