<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Supplier */

$this->title = $model->supplier_name;
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a
                        href="<?php echo Url::toRoute(['supplier/index']) ?>">Supplier</a></li>
            <li class="breadcrumb-item active" aria-current="page">View</li>
        </ol>
    </nav>
</div>

<div class="supplier-view">
    <?= $this->render('@app/views/User/_alert', ['module' => Yii::$app->getModule('user'),]) ?>

    <div class="card card-dark">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <div>
                        <h4 class="m-0 text-light"><i class="fas fa-shopping-cart mr-1"></i><?= $this->title ?></h4>
                    </div><!-- /.col -->
                </div>
                <div class="col-4">
                    <div class="float-right <?php echo (Yii::$app->user->can('manage-item')) ? 'visible' : 'invisible' ?>">
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger mt-1 mt-md-0',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'supplier_name',
                    'contact_persion',
                    'contact_firstname',
                    'address:ntext',
                    'phone_primary',
                    'phone_alternative',
                    'fax',
                    'email:email',
                ],
            ]) ?>
        </div>
    </div>

</div>
